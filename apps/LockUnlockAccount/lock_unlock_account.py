import logging
import os
import sys
import traceback

import pymysql
from dotenv import load_dotenv
from pymysql.constants.CLIENT import MULTI_STATEMENTS

load_dotenv()

connection = pymysql.connect(host=os.getenv('DATABASES_HOST'),
                             user=os.getenv('DATABASES_USER'),
                             passwd=os.getenv('DATABASES_PASSWORD'),
                             db=os.getenv('DATABASES_NAME'),
                             conv=pymysql.converters.conversions.copy(),
                             cursorclass=pymysql.cursors.DictCursor,
                             client_flag=MULTI_STATEMENTS,
                             connect_timeout=5)


def query_fetch_one(query, params):
    with connection.cursor() as cur:
        cur.execute(query, params)
        return cur.fetchone()


def is_email(email):
    return '@' in email


def is_lock(lock):
    return lock in ['lock', 'unlock']


def main():
    try:
        args = sys.argv[1:]
        email = None
        lock = None
        for a in args:
            if is_email(a):
                email = a
                continue

            if is_lock(a):
                lock = a
            else:
                print("lock/unlock invalid")
                return

        if email is None:
            print("Email is required.")
        if lock is None:
            print("Lock or Unlock is required.")

        result_set = query_fetch_one("SELECT status FROM gp_users where email = %s", (email,))

        if result_set is None:
            print("Not exit account")
        else:
            if result_set['status'] == 3 and lock == 'lock':
                mycursor = connection.cursor()
                sql = "Update gp_users set status = 2 where email = %s"
                val = email
                mycursor.execute(sql, val)
                connection.commit()
                print(mycursor.rowcount, "record(s) affected")
            elif result_set['status'] == 2 and lock == 'unlock':
                mycursor = connection.cursor()
                sql = "Update gp_users set status = 3 where email = %s"
                val = email
                mycursor.execute(sql, val)
                connection.commit()
                print(mycursor.rowcount, "record(s) affected")
            else:
                print("Not completed")
    except:
        logging.error("Error")
        logging.error(traceback.format_exc())


if __name__ == '__main__':
    main()
