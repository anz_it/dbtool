
import os
import datetime
import openpyxl
import logging
from pathlib import Path
from django.core.management.base import BaseCommand
from apps.User.models import Patient, User
from apps.Reminders.models import Reminders


def get_logging_config():
    pathFolder = str(Path().absolute())
    path = str(pathFolder + '/logs/reminders.log')
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.ERROR,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%d-%m-%Y %H:%M',
                        filename=path,
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    return logging


get_logging_config()


def write_log(messages):
    for message in messages:
        logging.error(message)


def isTimeFormat(input):
    print(input)
    if isinstance(input, datetime.time) is True:
        return True
    else:
        try:
            datetime.datetime.strptime(input, '%H:%M:%S')
            return True
        except ValueError as e:
            logging.error(str(e))
            return False


def validate_row_action_cards(count, row_data):
    messages = []
    id = row_data.get('A')
    patient_code = row_data.get('B')
    measurement_morning_time = str(row_data.get('C'))
    measurement_morning_onoff = row_data.get('D')
    measurement_evening_time = row_data.get('E')
    measurement_evening_onoff = row_data.get('F')
    medication_dose1_time = row_data.get('G')
    medication_dose1_onoff = row_data.get('H')
    medication_dose2_time = row_data.get('I')
    medication_dose2_onoff = row_data.get('J')
    medication_dose3_time = row_data.get('K')
    medication_dose3_onoff = row_data.get('L')
    medication_dose4_time = row_data.get('M')
    medication_dose4_onoff = row_data.get('N')
    notification = row_data.get('O')
    sync_date = row_data.get('P')

    # id
    if id is None:
        message = 'A' + str(count) + ' is not NULL'
        messages.append(message)

    if id is not None:
        try:
            id = int(id)
        except ValueError as e:
            message = 'A' + str(count) + ' is not character (string)'
            messages.append(message)

    # patient_code
    if patient_code is None:
        message = 'B' + str(count) + ' is not NULL'
        messages.append(message)
    else:
        try:
            Patient.objects.get(nhs_number=patient_code)
        except Patient.DoesNotExist:
            message = 'B' + str(count) + ' Patient does not exits'
            messages.append(message)

    # measurement_morning_time
    if measurement_morning_time is not None and isTimeFormat(measurement_morning_time) is False:
        message = 'C' + str(count) + ' Incorrect data format, should be H:M:S'
        messages.append(message)

    # measurement_morning_onoff
    if measurement_morning_onoff is not None:
        try:
            measurement_morning_onoff = int(measurement_morning_onoff)
        except ValueError as e:
            message = 'D' + str(count) + ' is not character (string)'
            messages.append(message)

    # measurement_evening_time
    if measurement_evening_time is not None and isTimeFormat(measurement_evening_time) is False:
        message = 'E' + str(count) + ' Incorrect data format, should be H:M:S'
        messages.append(message)

    # measurement_evening_onoff
    if measurement_evening_onoff is not None:
        try:
            measurement_evening_onoff = int(measurement_evening_onoff)
        except ValueError as e:
            message = 'F' + str(count) + ' is not character (string)'
            messages.append(message)

    # medication_dose1_time
    if medication_dose1_time is not None and isTimeFormat(medication_dose1_time) is False:
        message = 'G' + str(count) + ' Incorrect data format, should be H:M:S'
        messages.append(message)

    # medication_dose1_onoff
    if medication_dose1_onoff is not None:
        try:
            medication_dose1_onoff = int(medication_dose1_onoff)
        except ValueError as e:
            message = 'H' + str(count) + ' is not character (string)'
            messages.append(message)

    # medication_dose2_time
    if medication_dose2_time is not None and isTimeFormat(medication_dose2_time) is False:
        message = 'I' + str(count) + ' Incorrect data format, should be H:M:S'
        messages.append(message)

    # medication_dose2_onoff
    if medication_dose2_onoff is not None:
        try:
            medication_dose2_onoff = int(medication_dose2_onoff)
        except ValueError as e:
            message = 'J' + str(count) + ' is not character (string)'
            messages.append(message)

    # medication_dose3_time
    if medication_dose3_time is not None and isTimeFormat(medication_dose3_time) is False:
        message = 'K' + str(count) + 'Incorrect data format, should be H:M:S'
        messages.append(message)

    # medication_dose3_onoff
    if medication_dose3_onoff is not None:
        try:
            medication_dose3_onoff = int(medication_dose3_onoff)
        except ValueError as e:
            message = 'L' + str(count) + ' is not character (string)'
            messages.append(message)

    # medication_dose4_time
    if medication_dose4_time is not None and isTimeFormat(medication_dose4_time) is False:
        message = 'M' + str(count) + ' Incorrect data format, should be H:M:S'
        messages.append(message)

    # medication_dose4_onoff
    if medication_dose4_onoff is not None:
        try:
            medication_dose4_onoff = int(medication_dose4_onoff)
        except ValueError as e:
            message = 'N' + str(count) + ' is not character (string)'
            messages.append(message)

    # notification
    if notification is not None:
        try:
            notification = int(notification)
        except ValueError as e:
            message = 'O' + str(count) + ' is not character (string)'
            messages.append(message)

    # sync_date
    if sync_date is not None:
        if isinstance(sync_date, datetime.datetime) is False:
            try:
                datetime.datetime.strptime(sync_date, '%Y-%m-%d')
            except ValueError:
                message = 'O' + str(count) + ' Incorrect data format, should be YYYY-MM-DD'
                messages.append(message)

    if len(messages) > 0:
        write_log(messages)

    return messages


def process_reminders(data):
    count = 0
    for row in data:
        row_data = dict()
        count = count + 1
        if count > 1:
            for cell in row:
                name = cell.column_letter
                value_cell = cell.value
                row_data[name] = value_cell

            errors_row = validate_row_action_cards(count, row_data)
            if len(errors_row) == 0:
                id = row_data.get('A')
                patient_code = row_data.get('B')
                measurement_morning_time = str(row_data.get('C'))
                measurement_morning_onoff = row_data.get('D')
                measurement_evening_time = row_data.get('E')
                measurement_evening_onoff = row_data.get('F')
                medication_dose1_time = row_data.get('G')
                medication_dose1_onoff = row_data.get('H')
                medication_dose2_time = row_data.get('I')
                medication_dose2_onoff = row_data.get('J')
                medication_dose3_time = str(row_data.get('K'))
                medication_dose3_onoff = row_data.get('L')
                medication_dose4_time = row_data.get('M')
                medication_dose4_onoff = row_data.get('N')
                notification = row_data.get('O')
                sync_date = row_data.get('P')

                try:
                    patient = Patient.objects.get(nhs_number=patient_code)
                    data_insert = {
                        'patient_id': patient.id,
                        'measurement_morning_time': measurement_morning_time,
                        'measurement_morning_onoff': measurement_morning_onoff,
                        'measurement_evening_time': measurement_evening_time,
                        'measurement_evening_onoff': measurement_evening_onoff,
                        'medication_dose1_time': medication_dose1_time,
                        'medication_dose1_onoff': medication_dose1_onoff,
                        'medication_dose2_time': medication_dose2_time,
                        'medication_dose2_onoff': medication_dose2_onoff,
                        'medication_dose3_time': medication_dose3_time,
                        'medication_dose3_onoff': medication_dose3_onoff,
                        'medication_dose4_time': medication_dose4_time,
                        'medication_dose4_onoff': medication_dose4_onoff,
                        'notification': notification,
                        'sync_date': sync_date
                    }
                    print(patient.id)
                    obj = Reminders.objects.get(
                        patient_id=patient.id,
                    )
                    logging.info(data_insert)
                    for key, value in data_insert.items():
                        setattr(obj, key, value)
                    obj.save()
                    logging.info('Update :' + str(id))
                except Reminders.DoesNotExist:
                    logging.info('insert :' + str(id))
                    obj = Reminders(**data_insert)
                    obj.save()
            else:
                logging.info(row_data)


class Command(BaseCommand):
    help = 'Import reminders'

    def add_arguments(self, parser):
        parser.add_argument('-f', '--file_excel', type=str, help='file excel')

    def handle(self, *args, **kwargs):
        file_excel = kwargs['file_excel']
        if os.path.isfile(file_excel) is True:
            file_excel = kwargs['file_excel']
            wb = openpyxl.load_workbook(file_excel)
            worksheet_reminders = wb.active
            data_reminders = worksheet_reminders.iter_rows()
            process_reminders(data_reminders)
        else:
            logging.error('Path file is not exits!')
            self.stdout.write(self.style.ERROR('Path file is not exits!'))
