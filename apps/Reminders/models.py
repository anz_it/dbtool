from django.db import models
from datetime import datetime


class Reminders(models.Model):
    patient_id = models.IntegerField(primary_key=True)
    measurement_morning_time = models.TimeField(null=True)
    measurement_morning_onoff = models.IntegerField(null=True)
    measurement_evening_time = models.TimeField(null=True)
    measurement_evening_onoff = models.IntegerField(null=True)
    medication_dose1_time = models.TimeField(null=True)
    medication_dose1_onoff = models.IntegerField(null=True)
    medication_dose2_time = models.TimeField(null=True)
    medication_dose2_onoff = models.IntegerField(null=True)
    medication_dose3_time = models.TimeField(null=True)
    medication_dose3_onoff = models.IntegerField(null=True)
    medication_dose4_time = models.TimeField(null=True)
    medication_dose4_onoff = models.IntegerField(null=True)
    notification = models.IntegerField(null=True)
    sync_date = models.DateTimeField(null=True)
    created_date = models.DateTimeField(default=datetime.now, db_index=True)
    updated_date = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        db_table = "reminders"
