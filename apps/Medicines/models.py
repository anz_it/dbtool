"""Medicines"""
from datetime import datetime
from django.db import models
from apps.Ccg import consts

# Create your models here.
class Medicines(models.Model):
    """Medicines model"""
    medicine_id = models.BigIntegerField(primary_key=True)
    ccg_id = models.IntegerField()
    category_id = models.CharField(max_length=254)
    level = models.CharField(max_length=254)
    active_id = models.CharField(max_length=254)
    brand = models.CharField(max_length=254)
    form = models.CharField(max_length=254)
    dose = models.CharField(max_length=254)
    frequency = models.CharField(max_length=254)
    target_patient = models.IntegerField(choices=consts.TARGET_PATIENT, default=consts.TARGET_PATIENT.ADULT)
    titration_note_adults = models.TextField(max_length=1000)
    monitoring = models.TextField(max_length=1000)
    renal_impairment = models.TextField(max_length=1000)
    cautions = models.TextField(max_length=1000)
    diabetes = models.TextField(max_length=1000)
    heart_failure = models.TextField(max_length=1000)
    all_contraindications = models.TextField(max_length=1000)
    status = models.IntegerField()
    ordering = models.IntegerField()
    created_date = models.DateTimeField(default=datetime.now, db_index=True)
    updated_date = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        """Meta"""
        db_table = "medicines"
        def __ceil__(self):
            pass

        def __str__(self):
            return self.__class__.__name__
