from django.apps import AppConfig


class TableversionConfig(AppConfig):
    name = 'TableVersion'
