import datetime
import logging
import os
import shutil
import sys
import traceback
from pathlib import Path
import pandas as pd
import pymysql
from MySQLdb.constants.CLIENT import MULTI_STATEMENTS

from dotenv import load_dotenv

load_dotenv()

connection = pymysql.connect(host=os.getenv('DATABASES_HOST'),
                             user=os.getenv('DATABASES_USER'),
                             passwd=os.getenv('DATABASES_PASSWORD'),
                             db=os.getenv('DATABASES_NAME'),
                             conv=pymysql.converters.conversions.copy(),
                             cursorclass=pymysql.cursors.DictCursor,
                             client_flag=MULTI_STATEMENTS,
                             connect_timeout=5)


def get_logging_config():
    pathFolder = str(Path().absolute())
    path = str(pathFolder + '/logs/export_data_' + datetime.datetime.now().strftime('%d-%m-%Y') + '.log')
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.ERROR,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%d-%m-%Y %H:%M',
                        filename=path,
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    return logging


get_logging_config()


def execute(c, command):
    c.execute(command)
    return c.fetchall()


def is_table(table):
    return table in ['all', 'action_cards', 'answers', 'blood_pressures', 'ccgs', 'cycles', 'drug_intolerances', 'gp_users', 'hospitals','info_versions',
                     'medications', 'medicine_actives', 'medicines', 'medicines_categories', 'patient_infos', 'patient_questionnaires', 'patients', 'questionnaires',
                     'questions', 'reminders', 's_day_blood_pressures', 's_day_medications', 's_month_blood_pressures', 's_month_medications', 's_week_blood_pressures',
                     's_week_medications', 'treatment_medicines', 'treatment_steps', 'user_approvers']


def main():
    try:
        args = sys.argv[1:]
        table = None
        for a in args:
            if is_table(a):
                table = a
        if table is None:
            logging.error("Enter the table name or the character is 'all'")

        if table == 'all':
            c = connection.cursor()

            # get all table in database
            get_all_table = execute(c, "show tables;")

            for table in get_all_table:
                try:
                    sql = "Select * from " + list(table.values())[0] + ";"
                    results = pd.read_sql_query(sql, connection)
                    results.to_csv("exportexcel/" + list(table.values())[0] + ".csv", index=False)
                except:
                    logging.error("Error")
            print("Done")
            shutil.make_archive("all", "zip", 'exportexcel')
        else:
            try:
                sql = "Select * from " + table + ";"
                results = pd.read_sql_query(sql,connection)
                results.to_csv("exportexcel/" + table + ".csv", index=False)
                print("Done")
            except:
                logging.error("Not exit table name in database")

    except:
        logging.error("Error")
        logging.error(traceback.format_exc())


if __name__ == '__main__':
    main()

