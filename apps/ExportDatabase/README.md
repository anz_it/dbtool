#Export all table in database
#Mở terminal trong thư mục code : /home/tanpv/ominext/GP/3.4.source-tool-batch/apps/ExportDatabase
$ python export_data.py all
#Kết quả tất cả các bảng được xuất file 'tên bảng'.csv được lưu trong thư mục : /home/tanpv/ominext/GP/3.4.source-tool-batch/apps/ExportDatabase/exportexcel

#Trường hợp phát sinh lỗi thì lỗi được lưu trong thư mục : /home/tanpv/ominext/GP/3.4.source-tool-batch/apps/ExportDatabase/logs

#Export one table in database
#Mở terminal trong thư mục code : /home/tanpv/ominext/GP/3.4.source-tool-batch/apps/ExportDatabase
$ python export_data.py gp_users
#Kết quả bảng gp_users được xuất và lưu trong thư mục : /home/tanpv/ominext/GP/3.4.source-tool-batch/apps/ExportDatabase/exportexcel
#Các bảng còn lại cũng được thực hiện tương tự