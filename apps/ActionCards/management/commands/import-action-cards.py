from heapq import merge

import boto3
import botocore.exceptions
import hmac
import hashlib
import base64
import json
import os
import datetime
import re
import random
import string
import openpyxl
import logging
import pandas as pd
from pathlib import Path
from django.core.management.base import BaseCommand
from apps.Questions.models import Questionnaires, Questions, Answers
from apps.User.models import Patient, User
from apps.Hospitals.models import Hospitals
from apps.ActionCards.models import ActionCards


def get_logging_config():
    pathFolder = str(Path().absolute())
    path = str(pathFolder + '/logs/action-cards.log')
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.ERROR,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%d-%m-%Y %H:%M',
                        filename=path,
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    return logging


get_logging_config()


def write_log(messages):
    for message in messages:
        logging.error(message)


def validate_row_action_cards(count, row_data):
    messages = []
    id = row_data.get('A')
    patient_code = row_data.get('B')
    user_email = str(row_data.get('C'))
    action_type = row_data.get('D')
    ref_id = row_data.get('E')
    ref_table = row_data.get('F')
    action_date = row_data.get('G')
    actions_name = row_data.get('H')
    description = row_data.get('I')
    comments = row_data.get('J')
    status = str(row_data.get('K'))
    task_owner = row_data.get('L')
    start_date = row_data.get('M')
    end_date = row_data.get('N')
    sync_date = row_data.get('O')

    # id
    if id is None:
        message = 'A' + str(count) + ' is not NULL'
        messages.append(message)

    if id is not None:
        try:
            id = int(id)
        except ValueError as e:
            message = 'A' + str(count) + ' is not character (string)'
            messages.append(message)

    # patient_code
    if patient_code is None:
        message = 'B' + str(count) + ' is not NULL'
        messages.append(message)
    else:
        try:
            Patient.objects.get(nhs_number=patient_code)
        except Patient.DoesNotExist:
            message = 'B' + str(count) + ' Patient does not exits'
            messages.append(message)

    # user_email
    if user_email is None:
        message = 'C' + str(count) + ' is not NULL'
        messages.append(message)
    else:
        try:
            User.objects.filter(email=user_email).first()
        except User.DoesNotExist:
            message = 'C' + str(count) + 'GP User does not exits'
            messages.append(message)

    if user_email is not None and len(user_email) > 254:
        message = 'C' + str(count) + ' max length 254 character'
        messages.append(message)

    # action_type
    if action_type is None:
        message = 'D' + str(count) + ' is not NULL'
        messages.append(message)
    if action_type is not None:
        try:
            action_type = int(action_type)
        except ValueError as e:
            message = 'D' + str(count) + ' is not character (string)'
            messages.append(message)

    # ref_id
    if ref_id is not None:
        try:
            ref_id = int(ref_id)
        except ValueError as e:
            message = 'E' + str(count) + ' is not character (string)'
            messages.append(message)

    # ref_table
    if ref_table is not None and len(ref_table) > 254:
        message = 'F' + str(count) + ' max length 254 character'
        messages.append(message)

    # action_date
    if action_date is None:
        message = 'G' + str(count) + ' is not NULL'
        messages.append(message)
    if action_date is not None:
        if isinstance(action_date, datetime.datetime) is False:
            try:
                datetime.datetime.strptime(action_date, '%Y-%m-%d')
            except ValueError:
                message = 'G' + str(count) + 'Incorrect data format, should be YYYY-MM-DD'
                messages.append(message)

    # actions_name
    if actions_name is None:
        message = 'H' + str(count) + ' is not NULL'
        messages.append(message)
    if actions_name is not None and len(actions_name) > 254:
        message = 'H' + str(count) + ' max length 254 character'
        messages.append(message)

    # description
    if description is not None and len(description) > 4000:
        message = 'I' + str(count) + ' max length 4000 character'
        messages.append(message)

    # comments
    if comments is not None and len(comments) > 4000:
        message = 'J' + str(count) + ' max length 4000 character'
        messages.append(message)

    # status
    if status is None:
        message = 'K' + str(count) + ' is not NULL'
        messages.append(message)
    if status is not None:
        try:
            status = int(status)
        except ValueError as e:
            message = 'K' + str(count) + ' is not character (string)'
            messages.append(message)

    # task_owner
    if task_owner is None:
        message = 'L' + str(count) + ' is not NULL'
        messages.append(message)
    if task_owner is not None:
        try:
            task_owner = int(task_owner)
        except ValueError as e:
            message = 'L' + str(count) + ' is not character (string)'
            messages.append(message)

    # start_date
    if start_date is not None:
        if isinstance(start_date, datetime.datetime) is False:
            try:
                datetime.datetime.strptime(start_date, '%Y-%m-%d')
            except ValueError:
                message = 'M' + str(count) + 'Incorrect data format, should be YYYY-MM-DD'
                messages.append(message)

    # end_date
    if end_date is not None:
        if isinstance(end_date, datetime.datetime) is False:
            try:
                datetime.datetime.strptime(end_date, '%Y-%m-%d')
            except ValueError:
                message = 'N' + str(count) + 'Incorrect data format, should be YYYY-MM-DD'
                messages.append(message)

    # sync_date
    if sync_date is not None:
        if isinstance(sync_date, datetime.datetime) is False:
            try:
                datetime.datetime.strptime(sync_date, '%Y-%m-%d')
            except ValueError:
                message = 'O' + str(count) + 'Incorrect data format, should be YYYY-MM-DD'
                messages.append(message)

    if len(messages) > 0:
        write_log(messages)

    return messages


def process_action_cards(data):
    count = 0
    for row in data:
        row_data = dict()
        count = count + 1
        if count > 1:
            for cell in row:
                name = cell.column_letter
                value_cell = cell.value
                row_data[name] = value_cell

            errors_row = validate_row_action_cards(count, row_data)
            if len(errors_row) == 0:
                id = row_data.get('A')
                patient_code = row_data.get('B')
                user_email = str(row_data.get('C'))
                action_type = row_data.get('D')
                ref_id = row_data.get('E')
                ref_table = row_data.get('F')
                action_date = row_data.get('G')
                actions_name = row_data.get('H')
                description = row_data.get('I')
                comments = row_data.get('J')
                status = str(row_data.get('K'))
                task_owner = row_data.get('L')
                start_date = row_data.get('M')
                end_date = row_data.get('N')
                sync_date = row_data.get('O')

                try:
                    patient = Patient.objects.filter(nhs_number=patient_code).first()
                    gp_user = User.objects.filter(email=user_email).first()
                    data_insert = {
                        'id': id,
                        'patient_id': patient.id,
                        'gp_user_id': gp_user.id,
                        'action_type': action_type,
                        'ref_id': ref_id,
                        'ref_table': ref_table,
                        'action_date': action_date,
                        'action_name': actions_name,
                        'description': description,
                        'comments': comments,
                        'status': status,
                        'task_owner': task_owner,
                        'start_date': start_date,
                        'end_date': end_date,
                        'sync_date': sync_date,
                    }
                    obj = ActionCards.objects.get(
                        patient_id=patient.id,
                        gp_user_id=gp_user.id,
                    )
                    logging.info(data_insert)
                    for key, value in data_insert.items():
                        setattr(obj, key, value)
                    obj.save()
                    logging.info('Update :' + str(id))
                except ActionCards.DoesNotExist:
                    logging.info('insert :' + str(id))
                    obj = ActionCards(**data_insert)
                    obj.save()
            else:
                logging.info(row_data)


class Command(BaseCommand):
    help = 'Import Action Cards'

    def add_arguments(self, parser):
        parser.add_argument('-f', '--file_excel', type=str, help='file excel')

    def handle(self, *args, **kwargs):
        file_excel = kwargs['file_excel']
        if os.path.isfile(file_excel) is True:
            # sheet list_user
            file_excel = kwargs['file_excel']
            wb = openpyxl.load_workbook(file_excel)

            # QUESTIONNAIRES
            worksheet_action_cards = wb.active
            data_action_cards = worksheet_action_cards.iter_rows()
            process_action_cards(data_action_cards)
        else:
            logging.error('Path file is not exits!')
            self.stdout.write(self.style.ERROR('Path file is not exits!'))
