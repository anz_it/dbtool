from datetime import datetime

from django.db import models

# Create your models here.
class ActionCards(models.Model):
    id = models.AutoField(primary_key=True)
    patient_id = models.IntegerField()
    gp_user_id = models.IntegerField()
    action_type = models.IntegerField()
    ref_id = models.IntegerField()
    ref_table = models.CharField(max_length=254)
    action_date = models.DateField()
    action_name = models.CharField(max_length=254)
    description = models.TextField()
    comments = models.TextField()
    status = models.IntegerField(default=0)
    task_owner = models.IntegerField(default=0)
    start_date = models.DateField()
    end_date = models.DateField()
    sync_date = models.DateField()
    created_date = models.DateTimeField(default=datetime.now, db_index=True)
    updated_date = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        db_table = "action_cards"