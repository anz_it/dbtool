from django.apps import AppConfig


class ActioncardsConfig(AppConfig):
    name = 'ActionCards'
