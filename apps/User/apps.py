""" UserConfig """
from django.apps import AppConfig


class UserConfig(AppConfig):
    """ UserConfig """
    name = 'User'
