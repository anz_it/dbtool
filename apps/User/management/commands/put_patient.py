"""
pip requirement (install latest)
pandas
xlrd
"""
import logging
import traceback
import datetime
import json
from pathlib import Path
import numpy
import pandas as pd
import xlrd
import requests
from .get_connection import query_fetch_one, get_id_token
from apps.User.models import Patient


def get_logging_config():
    """
    get_logging_config
    :return: logging
    """
    path_folder = str(Path().absolute())
    now = datetime.datetime.now().strftime('%d-%m-%Y')
    path = str(path_folder + '/logs/account-patients-{}.log'.format(now))
    # set up logging to file - see previous section for more details
    logging.basicConfig(
        level=logging.ERROR,
        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        datefmt='%d-%m-%Y %H:%M',
        filename=path,
        filemode='w')
    return logging


logfile = get_logging_config()


def get_patient_id(email):
    """
    Check if patent exists in db
    Args:
        email(str): email of patient

    Returns:
        (int): id of patient if exist else raise CustomException
    """
    result_set = query_fetch_one(
        'SELECT patient_id FROM patients WHERE email = %s', email)
    if result_set is None:
        return None
    return result_set['patient_id']


def extract_data(filename) -> pd.core.groupby.generic.DataFrameGroupBy:
    """
    Read and merge data
    Args:
        filename(str): Name of file

    Returns:
        pandas.core.groupby.generic.DataFrameGroupBy: Merged data
    """
    patient_sheet = 'patients'
    patient_info_sheet = 'patient_infos'
    treatment_step_sheet = 'treatment_steps'
    treatment_medicine_sheet = 'treatment_medicines'
    # Read 4 sheets
    xlsx = pd.ExcelFile(filename)
    patient_data = pd.read_excel(
        xlsx,
        sheet_name=patient_sheet,
        keep_default_na=False)
    patient_info_data = pd.read_excel(
        xlsx,
        sheet_name=patient_info_sheet,
        keep_default_na=False)
    treatment_step_data = pd.read_excel(
        xlsx, sheet_name=treatment_step_sheet, keep_default_na=False)
    treatment_medicine_data = pd.read_excel(
        xlsx, sheet_name=treatment_medicine_sheet, keep_default_na=False)

    # patient data
    logging.info('Read patient data')
    patient_keep_field = [
        'user_id',
        'nhs_number',
        'account_status',
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'phone',
        'date_of_birth',
        'gender']
    patient_data = patient_data[patient_keep_field]  # Remove unused fields
    patient_data = patient_data.rename(columns={'user_id': 'hcp_approver_id'})
    patient_data['email'] = patient_data['email'].apply(
        lambda row: str(row).strip())
    duplicate_nhs = patient_data[patient_data.duplicated(
        subset=['nhs_number'], keep='first')]
    if not duplicate_nhs.empty:
        duplicate = [e for e in duplicate_nhs['nhs_number']]
        patient_data = patient_data.drop_duplicates(
            subset=['nhs_number'], keep='first')

    # patient_info data
    patient_info_keep_field = [
        'nhs_number',
        'ethnicity',
        'pregnant',
        'considering_pregnancy',
        'breastfeeding',
        'lastest_systolic',
        'lastest_diastolic',
        'lastest_pulse',
        'lastest_place_of_measurement',
        'lastest_date_of_measurement',
        'lastest_use_drug_treatment',
        'drug_intolerances_ace_inhibitor',
        'drug_intolerances_arb',
        'drug_intolerances_ccb',
        'drug_intolerances_thiazide_like_diuretic',
        'drug_intolerances_beta_blockers',
        'target_organ_damage',
        'established_cvd',
        'renal_disease',
        'diabetes',
        'estimated_10year',
        'chronic_kidney_disease',
        'evidence_of_heart_failure',
        'multimorbidity',
        'frailty',
        'postural',
    ]

    # Remove unused fields
    patient_info_data = patient_info_data[patient_info_keep_field]
    duplicate_info = patient_info_data[patient_info_data.duplicated(
        subset=['nhs_number'], keep='first')]
    if not duplicate_info.empty:
        duplicate = [i for i in duplicate_info['nhs_number']]
        patient_info_data = patient_info_data.drop_duplicates(
            subset=['nhs_number'], keep='first')

    # treatment_step data
    treatment_step_keep_field = [
        'nhs_number',
        'step_number',
        'target_diastolic',
        'target_systolic',
        'target_reason',
        'recommended_flag',
        'status',
        'deviated_comment',
        'reviewer_comment',
        'message_for_patient',
        'notes',
        'message_for_doctor',
    ]
    # Remove unused fields
    treatment_step_data = treatment_step_data[treatment_step_keep_field]
    duplicate_step = treatment_step_data[treatment_step_data.duplicated(
        subset=['nhs_number', 'step_number'], keep='first')]
    if not duplicate_step.empty:
        one = [d1 for d1 in duplicate_step['nhs_number']]
        two = [d2 for d2 in duplicate_step['step_number']]
        treatment_step_data = treatment_step_data.drop_duplicates(
            subset=['nhs_number', 'step_number'], keep='first')
    treatment_step_data = treatment_step_data.rename(
        columns={'id': 'step_id', 'status': 'treatment_step_status'})

    # treatment_medicine data
    treatment_medicine_keep_field = [
        'nhs_number',
        'step_number',
        'category',
        'level',
        'active',
        'brand',
        'form',
        'dose',
        'frequency',
        'status'
    ]
    # Remove unused fields
    treatment_medicine_data = treatment_medicine_data[treatment_medicine_keep_field]
    # treatment_medicine_data = treatment_medicine_data.rename(
    #     columns={'category': 'class'})
    # Merge data
    final_data = patient_data.merge(
        patient_info_data, how='inner', on='nhs_number')

    final_data = final_data.merge(
        treatment_step_data,
        how='inner',
        on='nhs_number')

    final_data = final_data.merge(
        treatment_medicine_data, how='inner', on=[
            'nhs_number', 'step_number'])

    # Post process
    final_data = final_data.where(pd.notnull(final_data), None)
    final_data = final_data.astype({'dose': float})

    # Group by patient
    final_data = final_data.groupby('nhs_number')
    return final_data


def send_request(url, token, json_data):
    """
    Send http request to API
    Args:
        url(str): API url
        json_data(dict): data

    Returns:
        None
    """
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + str(token)
    }
    response = requests.post(url=url, json=json_data, headers=headers)
    if not response.status_code == 200:
        message = json.loads(response.content).get('message')
        logfile.error(
            'Please check sheet treatment_steps, column nhs_number is : {} - {}'.format(json_data.get('nhs_number'), message))
        return False
    else:
        return response.content

def send_request_treatment_steps(url, token, json_data):
    """
    Send http request to API
    Args:
        url(str): API url
        json_data(dict): data

    Returns:
        None
    """
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + str(token)
    }
    response = requests.post(url=url, json=json_data, headers=headers)
    if not response.status_code == 200:
        message = json.loads(response.content).get('message')
        logfile.error(
            'Please check sheet treatment_steps, column nhs_number is : {} - {}'.format(json_data.get('nhs_number'), message))
        return False
    else:
        return response.content

def row_to_patient(row):
    """
    convert row to patient object
    :param row:
    :return:
    """
    result = dict()
    patient_field = [
        'hcp_approver_id',
        'nhs_number',
        'account_status',
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'phone',
        'date_of_birth',
        'gender',
        'ethnicity',
        'pregnant',
        'considering_pregnancy',
        'breastfeeding',
        'lastest_systolic',
        'lastest_diastolic',
        'lastest_pulse',
        'lastest_place_of_measurement',
        'lastest_date_of_measurement',
        'lastest_use_drug_treatment',
        'target_organ_damage',
        'established_cvd',
        'renal_disease',
        'diabetes',
        'estimated_10year',
        'chronic_kidney_disease',
        'evidence_of_heart_failure',
        'multimorbidity',
        'frailty',
        'postural']
    patient_field_list = [
        'drug_intolerances_ace_inhibitor',
        'drug_intolerances_arb',
        'drug_intolerances_ccb',
        'drug_intolerances_thiazide_like_diuretic',
        'drug_intolerances_beta_blockers',
    ]
    for field in patient_field:
        result[field] = cast_to_int(row[field].iloc[0])
    for field in patient_field_list:
        result[field] = [int(x.strip()) for x in row[field].iloc[0].split(',')]

    treatment_step_field = {
        'nhs_number': 'nhs_number',
        'step_number': 'step_number',
        'target_diastolic': 'target_diastolic',
        'target_systolic': 'target_systolic',
        'target_reason': 'target_reason',
        'recommended_flag': 'recommended_flag',
        'status': 'treatment_step_status',
        'deviated_comment': 'deviated_comment',
        'reviewer_comment': 'reviewer_comment',
        'message_for_patient': 'message_for_patient',
        'notes': 'notes',
        'message_for_doctor': 'message_for_doctor'
    }
    medication_field = [
        'category',
        'level',
        'active',
        'brand',
        'form',
        'frequency',
        'status'
    ]
    result['treatment_steps'] = []
    result['current_medications'] = []
    step_group = row.groupby(['step_number', 'nhs_number'])
    for _, step in step_group:
        new_step = {}
        for key, value in treatment_step_field.items():
            new_step[key] = cast_to_int(step[value].iloc[0])
        new_step['medications'] = []
        for key, value in step.iterrows():
            medicine = {}
            for medication in medication_field:
                if medication is 'frequency':
                    medicine[medication] = cast_to_int(value[medication])
                else:
                    medicine[medication] = str(value[medication])
            medicine['dose'] = str(value['dose'])  # Only dose is float
            new_step['medications'].append(medicine)
        if new_step['step_number'] == 0:
            result['current_medications'] = new_step['medications']
        else:
            result['treatment_steps'].append(new_step)
    return result


def cast_to_int(value):
    """
    convert to int
    :param value:
    :return:
    """
    if type(value) in [float, numpy.int64]:
        return int(value)
    return value


def process(url_patient_update, url_patient_treament_steps, filename):
    """
    process import datda
    :param url:
    :param filename:
    """
    try:
        token = get_id_token()
        if token is not None:
            data = extract_data(filename)
            for _, patient in data:
                patient_data = row_to_patient(patient)
                patient_id = get_patient_id(patient_data['email'])
                if patient_id is not None:
                    patient_data['patient_id'] = patient_id

                response = send_request(url=url_patient_update, token=token, json_data=patient_data)
                if response is not False:
                    treatment_steps = dict()
                    treatment_steps['treatment_steps'] = patient_data.get('treatment_steps')
                    patient = Patient.objects.get(email=patient_data['email'])
                    treatment_steps['id'] = patient.patient_id
                    response = send_request(url=url_patient_treament_steps, token=token, json_data=treatment_steps)

    except xlrd.biffh.XLRDError:
        logfile.error('Unsupported format, or corrupt file')
        logfile.error(traceback.format_exc())
    except KeyError:
        logfile.error('Missing some fields - Please check log')
        logfile.error(traceback.format_exc())
