# Yêu cầu cho file import dữ liệu
## Định dạng
 - Kiểu file xlsx
 - Có 4 sheet chứa các thông tin tương ứng (tên sheet lấy theo tên bảng trong db): patients, patient_infos, treatment_steps, treatment_medicines
## Các trường thông tin trong từng sheet
### patients
- user_id: int - id của bác sĩ phụ trách, tương đương hcp_approver_id
- nhs_number: string - độ dài = 10
- account_status: int - nằm trong đoạn [-1, 2]
- first_name: string
- middle_name: string
- last_name: string
- email:string
- phone: string - định dạng gồm dấu + ở đầu và có 1 khoảng trắng giữa mã quốc gia và số điện thoại. VD: +84 336689145
- date_of_birth: string - YY-MM-DD
- gender: int - {0,1}
### patient_infos
- nhs_number: int - id tham chiếu đến id sheet patients<br>
- ethnicity: int - {1,2,3}
- ethnicity: int - {0,1}
- considering_pregnancy: int - {0,1}
- breastfeeding: int - {0,1}
- lastest_systolic: int - [40, 280]
- lastest_diastolic: int - [40, 280]
- lastest_pulse: int - [1, 999]
- lastest_place_of_measurement: int - {1,2}
- lastest_date_of_measurement: string - YY-MM-DD
- lastest_use_drug_treatment, target_organ_damage, established_cvd, renal_disease, diabetes, estimated_10year, chronic_kidney_disease, evidence_of_heart_failure, multimorbidity, frailty, postural: int - {0,1}
- drug_intolerances_ace_inhibitor, drug_intolerances_arb, drug_intolerances_ccb, drug_intolerances_thiazide_like_diuretic: Mảng các string, các phần tử cách nhau bằng dấu phẩy. VD: A,B,C,D
### treatment_steps
- id: int - id của treatment step
- nhs_number: int - id tham chiếu đến patient
- <b>step_number: int - Tương ứng mỗi patient_id bắt buộc phải có 3 bản ghi với step_number =1, 2, 3, tuy nhiên nếu step_number = 0 thì medication tương ứng sẽ là current medication</b>	
- recommended_flag: int - {0,1}
- status: int - {0,1}
- message_for_patient, deviated_comment, reviewer_comment, notes, target_reason: string - có giới hạn số ký tự tối đa
- target_diastolic, target_systolic: int - [40, 280]
### treatment_medicines
- step_id: int - tham chiếu đến 1 treatment_steps qua id
- category: int - class của thuốc
- level, active, brand, form, frequency: int
- dose: double



