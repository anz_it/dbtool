""" command line import account dasboard """
import os
import datetime
import re
import random
import string
import logging
from pathlib import Path
import openpyxl
import boto3
from django.core.management.base import BaseCommand
from apps.User.models import User, UserApprover
from apps.Hospitals.models import Hospitals


def get_logging_config():
    """ get config log """

    path_folder = str(Path().absolute())
    path = str(path_folder + '/logs/account-dashboard-' +
               str(datetime.datetime.now().strftime('%d-%m-%Y')) + '.log')
    # set up logging to file - see previous section for more details
    logging.basicConfig(
        level=logging.ERROR,
        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        datefmt='%d-%m-%Y %H:%M',
        filename=path,
        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    return logging


logfile = get_logging_config()


def validate_hospital(count, row_data):
    """
    validate row excel
    :param count:
    :param row_data:
    :return: string or None
    """
    hospital = row_data.get('A')
    if hospital is None:
        message = 'SHEET LIST_USER: A' + str(count) + ' is not NULL'
        return message

    if len(hospital) > 30:
        message = 'SHEET LIST_USER: A' + \
                  str(count) + ' max length 30 character'
        return message

    try:
        Hospitals.objects.get(code=hospital)
    except Hospitals.DoesNotExist:
        message = 'SHEET LIST_USER: A' + str(count) + ' not found hospital'
        return message

    return None


def validate_first_name(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    first_name = row_data.get('B')
    if first_name is None:
        message = 'SHEET LIST_USER: B' + str(count) + ' is not NULL'
        return message

    if len(first_name) > 254:
        message = 'SHEET LIST_USER: B' + \
                  str(count) + ' max length 254 character'
        return message

    return None


def validate_middle_name(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    middle_name = row_data.get('C')
    if middle_name is not None and len(middle_name) > 254:
        message = 'SHEET LIST_USER: C' + \
                  str(count) + 'max length 254 character'
        return message
    return None


def validate_last_name(count, row_data):
    """
    validate_last_name
    :param count:
    :param row_data:
    :return: string or None
    """
    last_name = row_data.get('D')
    if last_name is None:
        message = 'SHEET LIST_USER: D' + str(count) + ' is not NULL'
        return message

    if len(last_name) > 254:
        message = 'SHEET LIST_USER: D' + \
                  str(count) + ' max length 254 character'
        return message

    return None


def validate_email(count, row_data):
    """
    validate_email
    :param count:
    :param row_data:
    :return: string or None
    """
    regex = r'[^@]+@[^@]+\.[^@]+'
    email = row_data.get('E')
    if email is None:
        message = 'SHEET LIST_USER: E' + str(count) + ' is not NULL'
        return message

    if len(email) > 254:
        message = 'SHEET LIST_USER: E' + \
                  str(count) + ' max length 254 character'
        return message

    if not re.search(regex, email):
        message = 'SHEET LIST_USER: E' + str(count) + ' Invalid Email'
        return message

    return None


def phone_validate(phone_number):
    """
    phone_validate
    :param phone_number:
    :return: True or False
    """
    try:
        int(phone_number)
        return True
    except ValueError:
        return False


def validate_phone(count, row_data):
    """
    validate_phone
    :param count:
    :param row_data:
    :return: string or None
    """
    phone = str(row_data.get('F'))
    if phone is None:
        message = 'SHEET LIST_USER: F' + str(count) + ' is not NULL'
        return message

    if len(phone) > 20:
        message = 'SHEET LIST_USER: F' + \
                  str(count) + ' max length 20 character'
        return message

    if phone_validate(phone) is False:
        message = 'SHEET LIST_USER: F' + \
                  str(count) + ' phone number is INT type'
        return message

    return None


def validate_country(count, row_data):
    """
    validate_country
    :param count:
    :param row_data:
    :return: string or None
    """
    country = row_data.get('G')
    if country is not None and len(country) > 254:
        message = 'SHEET LIST_USER: G' + \
                  str(count) + 'max length 254 character'
        return message

    return None


def validate_language(count, row_data):
    """
    validate_language
    :param count:
    :param row_data:
    :return: string or None
    """
    language = row_data.get('H')
    if language is not None and len(language) > 254:
        message = 'SHEET LIST_USER: H' + \
                  str(count) + 'max length 254 character'
        return message

    return None


def validate_status(count, row_data):
    """
    validate_status
    :param count:
    :param row_data:
    :return: string or None
    """
    status = row_data.get('I')
    if status is None:
        message = 'SHEET LIST_USER: I' + str(count) + ' is not NULL'
        return message

    try:
        status = int(status)
    except ValueError:
        message = 'SHEET LIST_USER: I' + \
                  str(count) + ' is not character (string)'
        return message

    return None


def validate_last_login(count, row_data):
    """
    validate_last_login
    :param count:
    :param row_data:
    :return: string or None
    """
    last_login = row_data.get('J')
    if last_login is not None:
        try:
            datetime.datetime.strptime(last_login, '%Y-%m-%d')
        except ValueError:
            message = 'SHEET LIST_USER: H' + \
                      str(count) + 'Incorrect data format, should be YYYY-MM-DD'
            return message

    return None


def validate_role(count, row_data):
    """
    validate_role
    :param count:
    :param row_data:
    :return: string or None
    """
    role = str(row_data.get('K'))
    if role is not None and len(role) > 254:
        message = 'SHEET LIST_USER: K' + \
                  str(count) + 'max length 254 character'
        return message

    return None


def validate_accepted_terms_version(count, row_data):
    """
    validate_accepted_terms_version
    :param count:
    :param row_data:
    :return: string or None
    """
    accepted_terms_version = row_data.get('L')
    if accepted_terms_version is not None and len(
            accepted_terms_version) > 254:
        message = 'SHEET LIST_USER: L' + \
                  str(count) + 'max length 254 character'
        return message

    return None


def validate_accepted_privacy_version(count, row_data):
    """
    validate_accepted_privacy_version
    :param count:
    :param row_data:
    :return: string or None
    """
    accepted_privacy_version = row_data.get('M')
    if accepted_privacy_version is not None and len(
            accepted_privacy_version) > 254:
        message = 'SHEET LIST_USER: M' + \
                  str(count) + 'max length 254 character'
        return message

    return None


def validate_accepted_privacy_statement(count, row_data):
    """
    validate_accepted_privacy_statement
    :param count:
    :param row_data:
    :return: string or None
    """
    accepted_privacy_statement = row_data.get('N')
    if accepted_privacy_statement is not None:
        try:
            int(accepted_privacy_statement)
        except ValueError:
            message = 'SHEET LIST_USER: N' + \
                      str(count) + ' is not character (string)'
            return message

    return None


def validate_accepted_promotion(count, row_data):
    """
    validate_accepted_promotion
    :param count:
    :param row_data:
    :return: string or None
    """
    accepted_promotion = row_data.get('O')
    if accepted_promotion is not None and accepted_promotion.isnumeric():
        message = 'SHEET LIST_USER: O' + str(count) + 'is not character'
        return message

    return None


def validate_active_date(count, row_data):
    """
    validate_active_date
    :param count:
    :param row_data:
    :return: string or None
    """
    active_date = row_data.get('P')
    if active_date is not None:
        try:
            datetime.datetime.strptime(active_date, '%Y-%m-%d')
        except ValueError:
            message = 'SHEET LIST_USER: P' + \
                      str(count) + 'Incorrect data format, should be YYYY-MM-DD'
            return message

    return None


def validate_row(count, row_data):
    """
    validate_row
    :param count:
    :param row_data:
    :return: string
    """
    messages = []
    message_hospital = validate_hospital(count, row_data)
    message_first_name = validate_first_name(count, row_data)
    message_middle_name = validate_middle_name(count, row_data)
    message_last_name = validate_last_name(count, row_data)
    message_email = validate_email(count, row_data)
    message_phone = validate_phone(count, row_data)
    message_country = validate_country(count, row_data)
    message_language = validate_language(count, row_data)
    message_status = validate_status(count, row_data)
    message_last_login = validate_last_login(count, row_data)
    message_role = validate_role(count, row_data)
    message_accepted_terms_version = validate_accepted_terms_version(
        count, row_data)
    message_accepted_privacy_version = validate_accepted_privacy_version(
        count, row_data)
    message_accepted_privacy_statement = validate_accepted_privacy_statement(
        count, row_data)
    message_accepted_promotion = validate_accepted_promotion(count, row_data)
    message_active_date = validate_active_date(count, row_data)

    if message_hospital is not None:
        messages.append(message_hospital)
    if message_first_name is not None:
        messages.append(message_first_name)
    if message_middle_name is not None:
        messages.append(message_middle_name)
    if message_last_name is not None:
        messages.append(message_last_name)
    if message_email is not None:
        messages.append(message_email)
    if message_phone is not None:
        messages.append(message_phone)
    if message_country is not None:
        messages.append(message_country)
    if message_language is not None:
        messages.append(message_language)
    if message_status is not None:
        messages.append(message_status)
    if message_last_login is not None:
        messages.append(message_last_login)
    if message_role is not None:
        messages.append(message_role)
    if message_accepted_terms_version is not None:
        messages.append(message_accepted_terms_version)
    if message_accepted_privacy_version is not None:
        messages.append(message_accepted_privacy_version)
    if message_accepted_privacy_statement is not None:
        messages.append(message_accepted_privacy_statement)
    if message_accepted_promotion is not None:
        messages.append(message_accepted_promotion)
    if message_active_date is not None:
        messages.append(message_active_date)

    if len(messages) > 0:
        write_log(logfile, messages)

    return messages


def validate_row_user_approver(count, row_data):
    """
    validate_row_user_approver
    :param count:
    :param row_data:
    :return: string messages
    """
    messages = []

    email_user = row_data.get('A')

    try:
        User.objects.get(email=email_user)
    except User.DoesNotExist:
        message = 'SHEET USER_APPROVERS: A' + str(count) + ' not found user'
        messages.append(message)

    email_user_approvers = row_data.get('B')
    try:
        User.objects.get(email=email_user_approvers)
    except User.DoesNotExist:
        message = 'SHEET USER_APPROVERS: B' + str(count) + ' not found user'
        messages.append(message)

    return messages


def random_password():
    """
    Random password
    :return: string
    """
    random_source = string.ascii_letters + string.digits + string.punctuation
    password = random.choice(string.ascii_lowercase)
    password += random.choice(string.ascii_uppercase)
    password += random.choice(string.digits)
    password += random.choice(string.punctuation)

    for i in range(6):
        password += random.choice(random_source) + str(i)

    password_list = list(password)
    random.SystemRandom().shuffle(password_list)
    password = ''.join(password_list)
    return password


def process_cognito(phone_number, email, count):
    """
    process_cognito
    :param phone_number:
    :param email:
    :return: string
    """
    client = boto3.client(
        'cognito-idp',
        aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
        region_name=os.getenv('COGNITO_AWS_REGION'))
    try:
        resp = client.sign_up(
            ClientId=os.getenv('COGNITO_APP_CLIENT_ID'),
            Username=email,
            Password=random_password(),
            UserAttributes=[
                {
                    'Name': "phone_number",
                    'Value': phone_number
                },
                {
                    'Name': "email",
                    'Value': email
                }
            ],
            ValidationData=[
                {
                    'Name': "email",
                    'Value': email
                },
                {
                    'Name': "phone_number",
                    'Value': phone_number
                }

            ])
    except client.exceptions.UsernameExistsException as error_messages:
        logging.error('SHEET LIST_USER: Row E%s This username already exists', str(count))
        return {
            "error": True,
        }
    except client.exceptions.InvalidPasswordException as error_messages:
        logging.error('SHEET LIST_USER: Row E%s', str(count))
        return {"error": True}
    except client.exceptions.UserLambdaValidationException as error_messages:
        logging.error('SHEET LIST_USER:  Row E%s', str(count))
        return {"error": True}

    except client.exceptions as error_messages:
        logging.exception(error_messages)
        logging.error(('SHEET LIST_USER: Row E%s ' + str(error_messages), str(count)))
        return {"error": True}

    return {"error": False, "data": resp}


def write_log(logging_file, messages):
    """
    write_log
    :param logging_file:
    :param messages:
    :return: None
    """
    for message in messages:
        logging_file.error(message)


def process_sheet_list_user(errors_row, data):
    """
    Process data file excel sheet LIST_USER and save to cognto AWS , table users
    :param errors_messages:
    :param data:
    :return: list
    """
    count = 0
    for row in data:
        row_data = dict()
        count = count + 1
        if count > 1:
            for cell in row:
                name = cell.column_letter
                value_cell = cell.value
                row_data[name] = value_cell

            errors_row = validate_row(count, row_data)
            if len(errors_row) == 0:
                email = row_data.get('E')
                phone_number = '+' + str(row_data.get('F'))
                data_insert = {
                    'hospital_id': Hospitals.objects.get(
                        code=row_data.get('A')).hospital_id,
                    'first_name': row_data.get('B'),
                    'middle_name': row_data.get('C'),
                    'last_name': row_data.get('D'),
                    'email': email,
                    'phone': phone_number,
                    'country': row_data.get('G'),
                    'language': row_data.get('H'),
                    'status': int(
                        row_data.get('I')),
                    'last_login_date': row_data.get('J'),
                    'login_count': 0,
                    'role': str(
                        row_data.get('K')),
                    'accepted_terms_version': row_data.get('L'),
                    'accepted_privacy_version': row_data.get('M'),
                    'accepted_privacy_statement': row_data.get('N'),
                    'accepted_promotion': row_data.get('O'),
                    'active_date': row_data.get('P'),
                }
                try:
                    obj = User.objects.get(email=email)
                    for key, value in data_insert.items():
                        setattr(obj, key, value)
                    obj.save()
                    data_cognito_response = process_cognito(
                        phone_number=phone_number, email=email, count=count)
                    if data_cognito_response.get('error') is True:
                        errors_row.append(data_cognito_response.get('message'))
                        continue
                except User.DoesNotExist:
                    data_cognito_response = process_cognito(
                        phone_number=phone_number, email=email, count=count)
                    if data_cognito_response.get('error') is True:
                        errors_row.append(data_cognito_response.get('message'))
                        continue

                    obj = User(**data_insert)
                    obj.gp_user_id = data_cognito_response.get('data').get('UserSub')
                    obj.save()
            else:
                write_log(logfile, errors_row)


def process_sheet_user_approver(errors_row, data1):
    """
    process data file excel -  sheet USER_APPROVERS and save to table user_approvers

    :param errors_messages:
    :param data1:
    :return: list message
    """
    count_approver = 0
    for row_approver in data1:
        row_data_user_approver = dict()
        count_approver = count_approver + 1

        if count_approver > 1:
            for cell in row_approver:
                name = cell.column_letter
                value_cell = cell.value
                row_data_user_approver[name] = value_cell

            errors_row = validate_row_user_approver(
                count_approver, row_data_user_approver)
            if len(errors_row) == 0:
                user_id = User.objects.get(
                    email=row_data_user_approver.get('B')).id
                hcp_approver_id = User.objects.get(
                    email=row_data_user_approver.get('C')).gp_user_id
                data_insert = {
                    'gp_user_id': user_id,
                    'hcp_approver_id': hcp_approver_id,
                    'status': 1,
                }
                try:
                    obj = UserApprover.objects.get(
                        user_id=user_id, hcp_approver_id=hcp_approver_id)
                    for key, value in data_insert.items():
                        setattr(obj, key, value)
                    obj.save()
                except UserApprover.DoesNotExist:
                    logging.info(data_insert)
                    obj = UserApprover(**data_insert)
                    obj.save()

    if len(errors_row) > 0:
        write_log(logfile, errors_row)

    return errors_row


class Command(BaseCommand):
    """Import Account Dashboard"""
    help = 'Import Account Dashboard'

    def add_arguments(self, parser):
        parser.add_argument('-f', '--file_excel', type=str, help='file excel')

    def handle(self, *args, **kwargs):
        file_excel = kwargs['file_excel']
        if os.path.isfile(file_excel) is True:
            errors_row = []
            try:
                worksheet = openpyxl.load_workbook(file_excel)
                # sheet list_user
                worksheet_list_user = worksheet["LIST_USER"]
                data_list_user = worksheet_list_user.iter_rows()

                # sheet list_user_approver
                worksheet_user_approver = worksheet["USER_APPROVERS"]
                data_user_approver = worksheet_user_approver.iter_rows()

                self.stdout.write(self.style.WARNING(
                    'Begin process sheet LIST_USER!'))
                process_sheet_list_user(errors_row, data_list_user)
                self.stdout.write(
                    self.style.WARNING('End process sheet LIST_USER!'))

                self.stdout.write(self.style.WARNING(
                    'Begin process sheet USER_APPROVERS!'))
                process_sheet_user_approver(errors_row, data_user_approver)
                self.stdout.write(self.style.WARNING(
                    'End process sheet USER_APPROVERS!'))

            except FileNotFoundError as message:
                errors_row.append(str(message))

            if len(errors_row) > 0:
                write_log(logfile, errors_row)

        else:
            logfile.error('Path file is not exits!')
            self.stdout.write(self.style.ERROR('Path file is not exits!'))
