"""connect sql AWS"""
import logging
import traceback
import os
import pymysql
import boto3

from pymysql.constants.CLIENT import MULTI_STATEMENTS


connection = pymysql.connect(host=os.getenv('DATABASES_HOST'),
                             user=os.getenv('DATABASES_USER'),
                             passwd=os.getenv('DATABASES_PASSWORD'),
                             db=os.getenv('DATABASES_NAME'),
                             conv=pymysql.converters.conversions.copy(),
                             cursorclass=pymysql.cursors.DictCursor,
                             client_flag=MULTI_STATEMENTS,
                             connect_timeout=5)


def query_fetch_one(query, params):
    """
    fetch data query
    :param query:
    :param params:
    :return: row
    """
    with connection.cursor() as cur:
        cur.execute(query, params)
        return cur.fetchone()

def get_id_token():
    """
        get token admin_initiate_auth
    :return: Token
    """
    try:
        client = boto3.client('cognito-idp',
                              aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                              aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
                              region_name=os.getenv('COGNITO_AWS_REGION'))

        response = client.initiate_auth(
            # UserPoolId=os.getenv('COGNITO_USER_POOL_ID'),
            ClientId=os.getenv('COGNITO_APP_CLIENT_ID'),
            AuthFlow='USER_PASSWORD_AUTH',
            AuthParameters={
                'USERNAME': os.getenv('AUTH_USER_NAME'),
                'PASSWORD': os.getenv('AUTH_PASSWORD')
            }
        )
        return response['AuthenticationResult']['IdToken']
    except: # pylint: disable=W0702
        logging.error('Cannot get validation token')
        logging.error(traceback.format_exc())
        return None
