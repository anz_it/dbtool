"""Import data patient"""
import datetime
import os
import logging
from pathlib import Path
from django.core.management.base import BaseCommand
from .put_patient import process


def get_logging_config():
    """
    get_logging_config
    :return:
    """
    path_folder = str(Path().absolute())
    now = datetime.datetime.now().strftime('%d-%m-%Y')
    path = str(path_folder + '/logs/account-dashboard-{}.log'.format(now))
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.ERROR,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%d-%m-%Y %H:%M',
                        filename=path,
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    return logging


logfile = get_logging_config()


class Command(BaseCommand):
    """Import Account Patients"""
    help = 'Import Account Patients'

    def add_arguments(self, parser):
        parser.add_argument('-f', '--file_excel', type=str, help='file excel')

    def handle(self, *args, **kwargs):
        url_patient_update = os.getenv('API_URL_PATIENT_UPDATE')
        url_patient_treament_steps = os.getenv('API_URL_TREATMENT_STEPS')
        file_excel = kwargs['file_excel']
        if os.path.isfile(file_excel) is True:
            file_excel = kwargs['file_excel']
            process(url_patient_update, url_patient_treament_steps, file_excel)
        else:
            logfile.error('Path file is not exits!')
            self.stdout.write(self.style.ERROR('Path file is not exits!'))
