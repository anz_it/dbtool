""" Module User """
from datetime import datetime
from django.db import models
from apps.Hospitals.models import Hospitals


# Create your models here.
class User(models.Model):
    """ Model GP User"""
    gp_user_id = models.CharField(primary_key=True,max_length=255)
    hospital = models.ForeignKey(Hospitals, on_delete=models.CASCADE, related_name='hospitals_user')
    first_name = models.CharField(max_length=255)
    middle_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    phone = models.CharField(max_length=20)
    country = models.CharField(max_length=255, blank=True, null=True)
    language = models.CharField(max_length=255, blank=True, null=True)
    status = models.IntegerField(default=0)
    last_login_date = models.DateTimeField(default=datetime.now, blank=True, null=True)
    login_count = models.IntegerField(blank=True, null=True)
    role = models.CharField(max_length=255, blank=True, null=True)
    accepted_terms_version = models.CharField(max_length=255, blank=True, null=True)
    accepted_privacy_version = models.CharField(max_length=255, blank=True, null=True)
    accepted_privacy_statement = models.CharField(max_length=255, blank=True, null=True)
    accepted_promotion = models.CharField(max_length=255, blank=True, null=True)
    active_date = models.DateTimeField(default=datetime.now, blank=True, null=True)
    created_date = models.DateTimeField(default=datetime.now, db_index=True, blank=True, null=True)
    updated_date = models.DateTimeField(auto_now=True, db_index=True, blank=True, null=True)

    class Meta:
        """Meta"""
        db_table = "gp_users"

        def __ceil__(self):
            pass

        def __str__(self):
            return self.__class__.__name__


# Create your models here.
class UserApprover(models.Model):
    """ Model UserApprover"""
    user_approver_id = models.AutoField(primary_key=True)
    gp_user_id = models.IntegerField()
    hcp_approver_id = models.IntegerField()
    status = models.IntegerField(default=1)
    created_date = models.DateTimeField(default=datetime.now, db_index=True, blank=True, null=True)
    updated_date = models.DateTimeField(auto_now=True, db_index=True, blank=True, null=True)

    class Meta:
        """Meta"""
        db_table = "user_approvers"

        def __ceil__(self):
            pass

        def __str__(self):
            return self.__class__.__name__


class Patient(models.Model):
    """ Model Patient"""
    patient_id = models.AutoField(primary_key=True)
    hospital = models.ForeignKey(Hospitals, on_delete=models.CASCADE, related_name='patient_hospitals')
    gp_user_id = models.IntegerField()
    nhs_number = models.CharField(max_length=20)
    account_status = models.IntegerField()
    first_name = models.CharField(max_length=254)
    middle_name = models.CharField(max_length=254)
    last_name = models.CharField(max_length=254)
    email = models.CharField(max_length=254)
    date_of_birth = models.DateField()
    gender = models.IntegerField()
    patient_status = models.IntegerField()
    priority = models.CharField(max_length=20)
    accepted_terms_version = models.CharField(max_length=254)
    accepted_privacy_version = models.CharField(max_length=254)
    accepted_promotion = models.IntegerField()
    last_login_date = models.DateTimeField()
    first_login_date = models.DateTimeField()
    home_tutotial_flag = models.IntegerField()
    medication_tutorial_flag = models.IntegerField()
    sync_date = models.DateTimeField()
    created_date = models.DateTimeField()
    updated_date = models.DateTimeField()

    class Meta:
        """Meta"""
        db_table = "patients"

        def __ceil__(self):
            pass

        def __str__(self):
            return self.__class__.__name__
