from django.conf.urls import url

from apps.User import views

urlpatterns = [
    url(r'^process_import_account_dashboard/$', views.process_import_account_dashboard, name='process_import_account_dashboard'),
]
