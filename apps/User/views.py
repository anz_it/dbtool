from heapq import merge

import boto3
import botocore.exceptions
import hmac
import hashlib
import base64
import json
import os
import datetime
import re
import random
import string

import openpyxl
from django.http import Http404, HttpResponse
from django.views.decorators.http import require_http_methods
from http import HTTPStatus
from apps.User.models import User, UserApprover
from apps.Hospitals.models import Hospitals


def get_secret_hash(username):
    msg = username + os.getenv('COGNITO_APP_CLIENT_ID')
    dig = hmac.new(str(os.getenv('COGNITO_APP_CLIENT_SECRET')).encode('utf-8'),
                   msg=str(msg).encode('utf-8'), digestmod=hashlib.sha256).digest()
    d2 = base64.b64encode(dig).decode()
    return d2

def validate_hospital(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    hospital = row_data.get('A')
    if hospital is None:
        message = 'SHEET LIST_USER: A' + str(count) + ' is not NULL'
        return message

    if len(hospital) > 30:
        message = 'SHEET LIST_USER: A' + str(count) + ' max length 30 character'
        return message

    try:
        Hospitals.objects.get(code=hospital)
    except Hospitals.DoesNotExist:
        message = 'SHEET LIST_USER: A' + str(count) + ' not found hospital'
        return message

    return None


def validate_first_name(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    first_name = row_data.get('B')
    if first_name is None:
        message = 'SHEET LIST_USER: B' + str(count) + ' is not NULL'
        return message

    if len(first_name) > 254:
        message = 'SHEET LIST_USER: B' + str(count) + ' max length 254 character'
        return message

    return None


def validate_middle_name(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    middle_name = row_data.get('C')
    if middle_name is not None and len(middle_name) > 254:
        message = 'SHEET LIST_USER: C' + str(count) + 'max length 254 character'
        return message
    return None


def validate_last_name(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    last_name = row_data.get('D')
    if last_name is None:
        message = 'SHEET LIST_USER: D' + str(count) + ' is not NULL'
        return message

    if len(last_name) > 254:
        message = 'SHEET LIST_USER: D' + str(count) + ' max length 254 character'
        return message

    return None


def validate_email(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
    email = row_data.get('E')
    if email is None:
        message = 'SHEET LIST_USER: E' + str(count) + ' is not NULL'
        return message

    if len(email) > 254:
        message = 'SHEET LIST_USER: E' + str(count) + ' max length 254 character'
        return message

    if not re.search(regex, email):
        message = 'SHEET LIST_USER: E' + str(count) + ' Invalid Email'
        return message

    return None


def validate_phone(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    phone = str(row_data.get('F'))
    if phone is None:
        message = 'SHEET LIST_USER: F' + str(count) + ' is not NULL'
        return message

    if len(phone) > 20:
        message = 'SHEET LIST_USER: F' + str(count) + ' max length 20 character'
        return message

    return None


def validate_country(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    country = row_data.get('G')
    if country is not None and len(country) > 254:
        message = 'SHEET LIST_USER: G' + str(count) + 'max length 254 character'
        return message

    return None


def validate_language(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    language = row_data.get('H')
    if language is not None and len(language) > 254:
        message = 'SHEET LIST_USER: H' + str(count) + 'max length 254 character'
        return message

    return None


def validate_status(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    status = row_data.get('I')
    if status is None:
        message = 'SHEET LIST_USER: I' + str(count) + ' is not NULL'
        return message

    try:
        status = int(status)
    except ValueError as e:
        message = 'SHEET LIST_USER: I' + str(count) + ' is not character (string)'
        return message

    return None


def validate_last_login(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    last_login = row_data.get('J')
    if last_login is not None:
        try:
            datetime.datetime.strptime(last_login, '%Y-%m-%d')
        except ValueError:
            message = 'SHEET LIST_USER: H' + str(count) + 'Incorrect data format, should be YYYY-MM-DD'
            return message

    return None


def validate_role(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    role = str(row_data.get('K'))
    if role is not None and len(role) > 254:
        message = 'SHEET LIST_USER: K' + str(count) + 'max length 254 character'
        return message

    return None


def validate_accepted_terms_version(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    accepted_terms_version = row_data.get('L')
    if accepted_terms_version is not None and len(accepted_terms_version) > 254:
        message = 'SHEET LIST_USER: L' + str(count) + 'max length 254 character'
        return message

    return None


def validate_accepted_privacy_version(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    accepted_privacy_version = row_data.get('M')
    if accepted_privacy_version is not None and len(accepted_privacy_version) > 254:
        message = 'SHEET LIST_USER: M' + str(count) + 'max length 254 character'
        return message

    return None


def validate_accepted_privacy_statement(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    accepted_privacy_statement = row_data.get('N')
    if accepted_privacy_statement is not None:
        try:
            accepted_privacy_statement = int(accepted_privacy_statement)
        except ValueError as e:
            message = 'SHEET LIST_USER: N' + str(count) + ' is not character (string)'
            return message

    return None


def validate_accepted_promotion(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    accepted_promotion = row_data.get('O')
    if accepted_promotion is not None and accepted_promotion.isnumeric():
        message = 'SHEET LIST_USER: O' + str(count) + 'is not character'
        return message

    return None


def validate_active_date(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string or None
    """
    active_date = row_data.get('P')
    if active_date is not None:
        try:
            datetime.datetime.strptime(active_date, '%Y-%m-%d')
        except ValueError:
            message = 'SHEET LIST_USER: P' + str(count) + 'Incorrect data format, should be YYYY-MM-DD'
            return message

    return None


def validate_row(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string
    """
    messages = []
    message_hospital = validate_hospital(count, row_data)
    message_first_name = validate_first_name(count, row_data)
    message_middle_name = validate_middle_name(count, row_data)
    message_last_name = validate_last_name(count, row_data)
    message_email = validate_email(count, row_data)
    message_phone = validate_phone(count, row_data)
    message_country = validate_country(count, row_data)
    message_language = validate_language(count, row_data)
    message_status = validate_status(count, row_data)
    message_last_login = validate_last_login(count, row_data)
    message_role = validate_role(count, row_data)
    message_accepted_terms_version = validate_accepted_terms_version(count, row_data)
    message_accepted_privacy_version = validate_accepted_privacy_version(count, row_data)
    message_accepted_privacy_statement = validate_accepted_privacy_statement(count, row_data)
    message_accepted_promotion = validate_accepted_promotion(count, row_data)
    message_active_date = validate_active_date(count, row_data)

    if message_hospital is not None:
        messages.append(message_hospital)
    if message_first_name is not None:
        messages.append(message_first_name)
    if message_middle_name is not None:
        messages.append(message_middle_name)
    if message_last_name is not None:
        messages.append(message_last_name)
    if message_email is not None:
        messages.append(message_email)
    if message_phone is not None:
        messages.append(message_phone)
    if message_country is not None:
        messages.append(message_country)
    if message_language is not None:
        messages.append(message_language)
    if message_status is not None:
        messages.append(message_status)
    if message_last_login is not None:
        messages.append(message_last_login)
    if message_role is not None:
        messages.append(message_role)
    if message_accepted_terms_version is not None:
        messages.append(message_accepted_terms_version)
    if message_accepted_privacy_version is not None:
        messages.append(message_accepted_privacy_version)
    if message_accepted_privacy_statement is not None:
        messages.append(message_accepted_privacy_statement)
    if message_accepted_promotion is not None:
        messages.append(message_accepted_promotion)
    if message_active_date is not None:
        messages.append(message_active_date)

    return messages


def validate_row_user_approver(count, row_data):
    messages = []
    hospital = row_data.get('A')
    try:
        obj_hospital = Hospitals.objects.get(code=hospital)
    except Hospitals.DoesNotExist:
        message = 'SHEET USER_APPROVERS: A' + str(count) + ' not found hospital'
        messages.append(message)

    email_user = row_data.get('B')

    try:
        obj_email_user = User.objects.get(email=email_user)
    except User.DoesNotExist:
        message = 'SHEET USER_APPROVERS: B' + str(count) + ' not found user'
        messages.append(message)

    email_user_approvers = row_data.get('C')
    try:
        obj_email_user_approvers = User.objects.get(email=email_user_approvers)
    except User.DoesNotExist:
        message = 'SHEET USER_APPROVERS: C' + str(count) + ' not found user'
        messages.append(message)

    return messages


def randomPassword():
    """
    Random password
    :return: string
    """
    randomSource = string.ascii_letters + string.digits + string.punctuation
    password = random.choice(string.ascii_lowercase)
    password += random.choice(string.ascii_uppercase)
    password += random.choice(string.digits)
    password += random.choice(string.punctuation)

    for i in range(6):
        password += random.choice(randomSource)

    passwordList = list(password)
    random.SystemRandom().shuffle(passwordList)
    password = ''.join(passwordList)
    return password


def process_cognito(phone_number, email):
    """

    :param phone_number:
    :param email:
    :return: string
    """
    client = boto3.client('cognito-idp', region_name=os.getenv('COGNITO_AWS_REGION'))
    try:
        resp = client.sign_up(
            ClientId=os.getenv('COGNITO_APP_CLIENT_ID'),
            SecretHash=get_secret_hash(email),
            Username=email,
            Password=randomPassword(),
            UserAttributes=[
                {
                    'Name': "phone_number",
                    'Value': phone_number
                },
                {
                    'Name': "email",
                    'Value': email
                }
            ],
            ValidationData=[
                {
                    'Name': "email",
                    'Value': email
                },
                {
                    'Name': "phone_number",
                    'Value': phone_number
                }

            ])

    except client.exceptions.UsernameExistsException as e:
        return {"error": True,
                "message": "This username already exists"}
    except client.exceptions.InvalidPasswordException as e:
        return {"error": True,
                "message": "Password should have Caps,\
                                  Special chars, Numbers"}
    except client.exceptions.UserLambdaValidationException as e:
        return {"error": True,
                "message": "Email already exists"}

    except Exception as e:
        return {"error": True,
                "message": str(e)}

    return {"error": False,
            "message": "Please confirm your signup, \
                            check Email for validation code"}


def process_sheet_list_user(errors_messages, data):
    """
    Process data file excel sheet LIST_USER and save to cognto AWS , table users

    :param errors_messages:
    :param data:
    :return: list
    """
    count = 0
    for row in data:
        row_data = dict()
        count = count + 1
        if count > 1:
            for cell in row:
                name = cell.column_letter
                value_cell = cell.value
                row_data[name] = value_cell

            errors_row = validate_row(count, row_data)
            if errors_row:
                errors_messages.append(errors_row)
            else:
                email = row_data.get('E')
                phone_number = '+' + str(row_data.get('F'))
                data_insert = {
                    'hospital_id': Hospitals.objects.get(code=row_data.get('A')).id,
                    'first_name': row_data.get('B'),
                    'middle_name': row_data.get('C'),
                    'last_name': row_data.get('D'),
                    'email': email,
                    'phone': phone_number,
                    'country': row_data.get('G'),
                    'language': row_data.get('H'),
                    'status': int(row_data.get('I')),
                    'last_login': row_data.get('J'),
                    'login_count': 0,
                    'role': str(row_data.get('K')),
                    'accepted_terms_version': row_data.get('L'),
                    'accepted_privacy_version': row_data.get('M'),
                    'accepted_privacy_statement': row_data.get('N'),
                    'accepted_promotion': row_data.get('O'),
                    'active_date': row_data.get('P'),
                }
                try:
                    obj = User.objects.get(email=email)
                    for key, value in data_insert.items():
                        setattr(obj, key, value)
                    obj.save()
                except User.DoesNotExist:
                    data_cognito_response = process_cognito(phone_number=phone_number, email=email)
                    if data_cognito_response.get('error') is True:
                        errors_row.append(data_cognito_response.get('message'))
                        errors_messages.append(errors_row)
                        continue

                    obj = User(**data_insert)
                    obj.save()
    return errors_messages


def process_sheet_user_approver(errors_messages, data1):
    """
    process data file excel -  sheet USER_APPROVERS and save to table user_approvers

    :param errors_messages:
    :param data1:
    :return: list message
    """
    count_approver = 0
    for row_approver in data1:
        row_data_user_approver = dict()
        count_approver = count_approver + 1
        if count_approver > 1:
            for cell in row_approver:
                name = cell.column_letter
                value_cell = cell.value
                row_data_user_approver[name] = value_cell

            errors_row_user_approver = validate_row_user_approver(count_approver, row_data_user_approver)
            if errors_row_user_approver:
                errors_messages.append(errors_row_user_approver)
            else:
                hospital_id = Hospitals.objects.get(code=row_data_user_approver.get('A')).id
                user_id = User.objects.get(email=row_data_user_approver.get('B')).id
                hcp_approver_id = User.objects.get(email=row_data_user_approver.get('C')).id
                data_insert = {
                    'hospital_id': hospital_id,
                    'user_id': user_id,
                    'hcp_approver_id': hcp_approver_id,
                    'status': 1,
                }
                try:
                    obj = UserApprover.objects.get(user_id=user_id, hcp_approver_id=hcp_approver_id)
                    for key, value in data_insert.items():
                        setattr(obj, key, value)
                    obj.save()
                except UserApprover.DoesNotExist:
                    obj = UserApprover(**data_insert)
                    obj.save()

    return errors_messages


@require_http_methods(["POST"])
def process_import_account_dashboard(request):
    if request.is_ajax():
        errors_messages = []
        errors_row = []
        try:
            excel_file = request.FILES['excel_file']
            wb = openpyxl.load_workbook(excel_file)
            # getting a particular sheet by name out of many sheets
            worksheet_list_user = wb["LIST_USER"]
            data = worksheet_list_user.iter_rows()
            worksheet_user_approver = wb["USER_APPROVERS"]
            data1 = worksheet_user_approver.iter_rows()
        except FileNotFoundError as message:
            errors_row.append(str(message))
            errors_messages.append(errors_row)
            return HttpResponse(json.dumps(errors_messages), content_type='application/json',
                                status=HTTPStatus.INTERNAL_SERVER_ERROR)
        except Exception as message:
            errors_row.append(str(message))
            errors_messages.append(errors_row)
            return HttpResponse(json.dumps(errors_messages), content_type='application/json',
                                status=HTTPStatus.INTERNAL_SERVER_ERROR)

        errors_messages = list(
            merge(process_sheet_list_user(errors_messages, data), process_sheet_user_approver(errors_messages, data1)))

        return HttpResponse(json.dumps(errors_messages), content_type='application/json', status=HTTPStatus.OK)
    else:
        raise Http404
