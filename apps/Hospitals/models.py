from django.db import models
from datetime import datetime, date
from apps.Ccg.models import Ccg


# Create your models here.
class Hospitals(models.Model):
    hospital_id = models.AutoField(primary_key=True)
    ccg = models.ForeignKey(Ccg, on_delete=models.CASCADE, related_name='hospital_ccg')
    code = models.CharField(max_length=30)
    name = models.CharField(max_length=254)
    address = models.CharField(max_length=254)
    open_time = models.CharField(max_length=8)
    close_time = models.CharField(max_length=8)
    office_contact = models.CharField(max_length=254)
    out_of_hours_contact = models.CharField(max_length=254)
    sync_date = models.DateTimeField()
    status = models.IntegerField()
    created_date = models.DateTimeField(default=datetime.now, db_index=True)
    updated_date = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        db_table = "hospitals"
