from heapq import merge

import boto3
import botocore.exceptions
import hmac
import hashlib
import base64
import json
import os
import datetime
import re
import random
import string
import openpyxl
import logging
from pathlib import Path

from apps.Ccg.models import Ccg
from apps.User.models import User, UserApprover
from apps.Hospitals.models import Hospitals
from django.core.management.base import BaseCommand


def get_logging_config():
    pathFolder = str(Path().absolute())
    path = str(pathFolder + '/logs/import-excel-hospitals.log')
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.ERROR,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%d-%m-%Y %H:%M',
                        filename=path,
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    return logging


logfile = get_logging_config()


def write_log(logfile, messages):
    for message in messages:
        logfile.error(message)


def validate_ccg_code(count, row_data):
    """
       :param count:
       :param row_data:
       :return: string or None
       """
    ccg_code = row_data.get('A')
    if ccg_code is None:
        message = 'SHEET CCGs: A' + str(count) + ' is not NULL'
        return message

    if len(ccg_code) > 50:
        message = 'SHEET CCGs: A' + str(count) + ' max length 50 character'
        return message

    return None


def validate_ccg_name(count, row_data):
    """
       :param count:
       :param row_data:
       :return: string or None
       """
    ccg_name = row_data.get('B')
    if ccg_name is None:
        message = 'SHEET CCGs: B' + str(count) + ' is not NULL'
        return message

    if len(ccg_name) > 255:
        message = 'SHEET CCGs: B' + str(count) + ' max length 255 character'
        return message

    return None


def validate_hospital_code(count, row_data):
    """
       :param count:
       :param row_data:
       :return: string or None
       """
    hospital_code = row_data.get('B')
    if hospital_code is None:
        message = 'SHEET HOSPITALS: B' + str(count) + ' is not NULL'
        return message

    if len(hospital_code) > 255:
        message = 'SHEET HOSPITALS: B' + str(count) + ' max length 255 character'
        return message

    return None


def validate_hospital_name(count, row_data):
    """
       :param count:
       :param row_data:
       :return: string or None
       """
    hospital_name = row_data.get('C')
    if hospital_name is None:
        message = 'SHEET HOSPITALS: C' + str(count) + ' is not NULL'
        return message

    if len(hospital_name) > 255:
        message = 'SHEET HOSPITALS: C' + str(count) + ' max length 255 character'
        return message

    return None


def validate_hospital_address(count, row_data):
    """
       :param count:
       :param row_data:
       :return: string or None
       """
    hospital_address = row_data.get('D')
    if hospital_address is not None and len(hospital_address) > 255:
        message = 'SHEET HOSPITALS: D' + str(count) + ' max length 255 character'
        return message

    return None


def validate_open_time(count, row_data):
    """
       :param count:
       :param row_data:
       :return: string or None
       """
    open_time = row_data.get('E')
    if open_time is not None and len(open_time) > 255:
        message = 'SHEET HOSPITALS: E' + str(count) + ' max length 255 character'
        return message

    return None


def validate_close_time(count, row_data):
    """
       :param count:
       :param row_data:
       :return: string or None
       """
    close_time = row_data.get('F')
    if close_time is not None and len(close_time) > 255:
        message = 'SHEET HOSPITALS: F' + str(count) + ' max length 255 character'
        return message

    return None


def validate_office_contact(count, row_data):
    """
       :param count:
       :param row_data:
       :return: string or None
       """
    office_contact = row_data.get('G')
    if office_contact is not None and len(office_contact) > 255:
        message = 'SHEET HOSPITALS: G' + str(count) + ' max length 255 character'
        return message

    return None


def validate_out_of_hours_contact(count, row_data):
    """
       :param count:
       :param row_data:
       :return: string or None
       """
    out_of_hours_contact = row_data.get('H')
    if out_of_hours_contact is not None and len(out_of_hours_contact) > 255:
        message = 'SHEET HOSPITALS: H' + str(count) + ' max length 255 character'
        return message

    return None


def validate_hospital_status(count, row_data):
    """
       :param count:
       :param row_data:
       :return: string or None
       """
    ccg_status = row_data.get('I')

    if ccg_status is not None:
        try:
            ccg_status = int(ccg_status)
        except:
            message = 'SHEET HOSPITALS: I' + str(count) + ' The format must be an Integer Type'
            return message

        return None


def validate_sync_date(count, row_data):
    """
       :param count:
       :param row_data:
       :return: string or None
       """
    out_of_hours_contact = row_data.get('J')
    if out_of_hours_contact is not None:
        try:
            datetime.datetime.strptime(out_of_hours_contact, '%d/%m/%Y')
        except ValueError:
            message = 'SHEET HOSPITALS: J' + str(count) + ' Incorrect data format, should be DD-MM-YYYY'
            return message

    return None


def validate_ccg_status(count, row_data):
    """
       :param count:
       :param row_data:
       :return: string or None
       """
    ccg_status = row_data.get('C')

    if ccg_status is None:
        message = 'SHEET CCGs: C' + str(count) + ' is not NULL'
        return message

    try:
        ccg_status = int(ccg_status)
    except:
        message = 'SHEET CCGs: C' + str(count) + ' The format must be an Integer Type'
        return message

    return None


def validate_row_ccg(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string
    """
    messages = []
    message_code = validate_ccg_code(count, row_data)
    message_name = validate_ccg_name(count, row_data)
    message_status = validate_ccg_status(count, row_data)

    if message_code is not None:
        messages.append(message_code)

    if message_name is not None:
        messages.append(message_name)

    if message_status is not None:
        messages.append(message_status)

    if len(messages) > 0:
        write_log(logfile, messages)

    return messages


def process_worksheet_list_ccg(worksheet_list_ccg):
    count = 0
    for row in worksheet_list_ccg:
        row_data = dict()
        count = count + 1
        if count > 1:
            for cell in row:
                name = cell.column_letter
                value_cell = cell.value
                row_data[name] = value_cell

            errors_row = validate_row_ccg(count, row_data)
            if len(errors_row) == 0:
                data_insert = {
                    'code': row_data.get('A'),
                    'name': row_data.get('B'),
                    'status': int(row_data.get('C')),
                }
                print(data_insert)
                try:
                    obj = Ccg.objects.get(code=row_data.get('A'))
                    for key, value in data_insert.items():
                        setattr(obj, key, value)
                    obj.save()
                except Ccg.DoesNotExist:
                    obj = Ccg(**data_insert)
                    obj.save()


def validate_row_hospital(count, row_data):
    """

    :param count:
    :param row_data:
    :return: string
    """
    messages = []
    message_code = validate_ccg_code(count, row_data)
    message_hospital_code = validate_hospital_code(count, row_data)
    message_hospital_name = validate_hospital_name(count, row_data)
    message_hospital_address = validate_hospital_address(count, row_data)
    message_open_time = validate_open_time(count, row_data)
    message_close_time = validate_close_time(count, row_data)
    message_office_contact = validate_office_contact(count, row_data)
    message_out_of_hours_contact = validate_out_of_hours_contact(count, row_data)
    message_hospital_status = validate_hospital_status(count, row_data)
    message_sync_date = validate_sync_date(count, row_data)

    if message_code is not None:
        messages.append(message_code)

    if message_hospital_code is not None:
        messages.append(message_hospital_code)

    if message_hospital_name is not None:
        messages.append(message_hospital_name)

    if message_hospital_address is not None:
        messages.append(message_hospital_address)

    if message_open_time is not None:
        messages.append(message_open_time)

    if message_close_time is not None:
        messages.append(message_close_time)

    if message_office_contact is not None:
        messages.append(message_office_contact)

    if message_out_of_hours_contact is not None:
        messages.append(message_out_of_hours_contact)

    if message_hospital_status is not None:
        messages.append(message_hospital_status)

    if message_sync_date is not None:
        messages.append(message_sync_date)

    if len(messages) > 0:
        write_log(logfile, messages)

    return messages


def process_worksheet_list_hospital(worksheet_data_hospitals):
    count = 0
    for row in worksheet_data_hospitals:
        row_data = dict()
        count = count + 1
        if count > 1:
            for cell in row:
                name = cell.column_letter
                value_cell = cell.value
                row_data[name] = value_cell

            errors_row = validate_row_hospital(count, row_data)
            if len(errors_row) == 0:
                ccg = Ccg.objects.get(code=row_data.get('A'))
                data_insert = {
                    'ccg_id': ccg.id,
                    'code': row_data.get('B'),
                    'name': row_data.get('C'),
                    'address': row_data.get('D'),
                    'open_time': row_data.get('E'),
                    'close_time': row_data.get('F'),
                    'office_contact': row_data.get('G'),
                    'out_of_hours_contact': row_data.get('H'),
                    'status': row_data.get('I'),
                    'sync_date': row_data.get('J'),
                }
                try:
                    obj = Hospitals.objects.get(code=row_data.get('B'))
                    print('update')
                    print(data_insert)
                    for key, value in data_insert.items():
                        setattr(obj, key, value)
                    obj.save()
                except Hospitals.DoesNotExist:
                    print('insert')
                    obj = Hospitals(**data_insert)
                    obj.save()


class Command(BaseCommand):
    help = 'Import Excels CCG and Hospitals'

    def add_arguments(self, parser):
        parser.add_argument('-f', '--file_excel', type=str, help='file excel')

    def handle(self, *args, **kwargs):
        global worksheet, ccg
        file_excel = kwargs['file_excel']
        if os.path.isfile(file_excel) is True:
            errors_row = []
            try:
                wb = openpyxl.load_workbook(file_excel)
                # sheet CCGs
                worksheet_list_ccg = wb["CCGs"]
                data_ccgs = worksheet_list_ccg.iter_rows()
                process_worksheet_list_ccg(data_ccgs)
                # sheet HOSPITAL
                worksheet_list_hospital = wb["HOSPITALS"]
                data_hospitals = worksheet_list_hospital.iter_rows()
                process_worksheet_list_hospital(data_hospitals)

            except FileNotFoundError as message:
                errors_row.append(str(message))
            except Exception as message:
                errors_row.append(str(message))

        else:
            logfile.error('Path file is not exits!')
            self.stdout.write(self.style.ERROR('Path file is not exits!'))
