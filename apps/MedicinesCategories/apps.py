from django.apps import AppConfig


class MedicinescategoriesConfig(AppConfig):
    name = 'MedicinesCategories'
