import os
import datetime
import logging
from pathlib import Path

import openpyxl
from django.core.management.base import BaseCommand
from apps.Questions.models import Questionnaires, Questions, Answers
from apps.User.models import Patient


def get_logging_config() -> object:
    """
    get_logging_config
    :return: object
    """
    path_folder = str(Path().absolute())
    now = datetime.datetime.now().strftime('%d-%m-%Y')
    path = str(path_folder + '/logs/import-questions-{}.log').format(now)
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.ERROR,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%d-%m-%Y %H:%M',
                        filename=path,
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    return logging


get_logging_config()


def write_log(messages):
    """
    write_log
    :param messages:
    :return:
    """
    for message in messages:
        logging.error(message)


def validate_row_questionnaires(count, row_data):
    """
    validate_row_questionnaires
    :param count:
    :param row_data:
    :return:
    """
    messages = []
    version = row_data.get('B')
    type_questionnaires = row_data.get('C')
    status = row_data.get('D')

    if version is None:
        message = 'SHEET QUESTIONNAIRES: B' + str(count) + ' is not NULL'
        messages.append(message)

    if version is not None and len(version) > 20:
        message = 'SHEET QUESTIONNAIRES: B' + str(count) + ' max length 20 character'
        messages.append(message)

    if type_questionnaires is None:
        message = 'SHEET QUESTIONNAIRES: C' + str(count) + ' is not NULL'
        messages.append(message)

    if type_questionnaires is not None and type(type_questionnaires) not in [int]:
        message = 'SHEET QUESTIONNAIRES: C' + str(count) + ' is not character (string)'
        messages.append(message)

    if type_questionnaires is not None and type_questionnaires not in [1, 2]:
        message = 'SHEET QUESTIONNAIRES: C' + str(count) + ' type must be in {-1,1}'
        messages.append(message)

    if status is None:
        message = 'SHEET QUESTIONNAIRES: D' + str(count) + ' is not NULL'
        messages.append(message)

    if status is not None and type(status) not in [int]:
        message = 'SHEET QUESTIONNAIRES: D' + str(count) + ' is not character (string)'
        messages.append(message)

    if status is not None and status not in [-1, 0, 1]:
        message = 'SHEET QUESTIONNAIRES: D' + str(count) + ' status must be in {-1,1}'
        messages.append(message)

    if len(messages) > 0:
        write_log(messages)

    return messages


def process_sheet_questionnaires(data):
    """
    process_sheet_questionnaires
    :param data:
    :return:
    """
    count = 0
    for row in data:
        row_data = dict()
        count = count + 1
        if count > 1:
            for cell in row:
                name = cell.column_letter
                value_cell = cell.value
                row_data[name] = value_cell

            errors_row = validate_row_questionnaires(count, row_data)

            if len(errors_row) == 0:
                questionnaire_id = row_data.get('A')
                version = row_data.get('B')
                type_questionnaire = row_data.get('C')
                status = row_data.get('D')

                data_insert = {
                    'version': version,
                    'type': type_questionnaire,
                    'status': status,
                }
                try:
                    obj = Questionnaires.objects.get(questionnaire_id=questionnaire_id)
                    for key, value in data_insert.items():
                        setattr(obj, key, value)
                    obj.save()
                except Questionnaires.DoesNotExist:
                    obj = Questionnaires(**data_insert)
                    obj.save()
            else:
                logging.info(row_data)


def validate_row_questions(count, row_data):
    """
    validate_row_questions
    :param count:
    :param row_data:
    :return:
    """
    messages = []
    question_id = row_data.get('A')
    questionnaire_id = row_data.get('B')
    language = row_data.get('C')
    description = row_data.get('D')
    title_code = str(row_data.get('E'))
    title_name = row_data.get('F')
    question = row_data.get('G')
    title_ordering = row_data.get('H')
    question_code = str(row_data.get('I'))
    question_type = row_data.get('J')
    question_label = row_data.get('K')
    sub_question_code = str(row_data.get('L'))
    sub_question = row_data.get('M')
    question_ordering = row_data.get('N')
    option_code = str(row_data.get('O'))
    option_name = row_data.get('P')
    required_flag = row_data.get('Q')
    option_ordering = row_data.get('R')

    # id
    if question_id is None:
        message = 'SHEET QUESTIONS: A' + str(count) + ' is not NULL'
        messages.append(message)

    # id
    if question_id is not None and type(question_id) not in [int]:
        message = 'SHEET QUESTIONS: A' + str(count) + ' is not character (string)'
        messages.append(message)

    # questionnaire_id
    if questionnaire_id is None:
        message = 'SHEET QUESTIONS: B' + str(count) + ' is not NULL'
        messages.append(message)

    # questionnaire_id
    if questionnaire_id is not None and type(questionnaire_id) not in [int]:
        message = 'SHEET QUESTIONS: B' + str(count) + ' is not character (string)'
        messages.append(message)

    # language
    if language is None:
        message = 'SHEET QUESTIONS: C' + str(count) + ' is not NULL'
        messages.append(message)

    # language
    if language is not None and len(language) > 5:
        message = 'SHEET QUESTIONS: C' + str(count) + ' max length 5 character'
        messages.append(message)

    # description
    if description is None:
        message = 'SHEET QUESTIONS: D' + str(count) + ' is not NULL'
        messages.append(message)

    # description
    if description is not None and len(description) > 1000:
        message = 'SHEET QUESTIONS: D' + str(count) + ' max length 1000 character'
        messages.append(message)

    # title_code
    if title_code is None:
        message = 'SHEET QUESTIONS: E' + str(count) + ' is not NULL'
        messages.append(message)

    # title_code
    if title_code is not None and len(title_code) > 50:
        message = 'SHEET QUESTIONS: E' + str(count) + ' max length 50 character'
        messages.append(message)

    # title_name
    if title_name is None:
        message = 'SHEET QUESTIONS: F' + str(count) + ' is not NULL'
        messages.append(message)

    if title_name is not None and len(title_name) > 100:
        message = 'SHEET QUESTIONS: F' + str(count) + ' max length 100 character'
        messages.append(message)

    # question
    if question is None:
        message = 'SHEET QUESTIONS: G' + str(count) + ' is not NULL'
        messages.append(message)

    if question is not None and len(question) > 254:
        message = 'SHEET QUESTIONS: G' + str(count) + ' max length 254 character'
        messages.append(message)

    # title_ordering
    if title_ordering is not None and type(title_ordering) not in [int]:
        message = 'SHEET QUESTIONS: H' + str(count) + ' is not character (string)'
        messages.append(message)

    # question_code
    if question_code is None:
        message = 'SHEET QUESTIONS: I' + str(count) + ' is not NULL'
        messages.append(message)

    if question_code is not None and len(question_code) > 50:
        message = 'SHEET QUESTIONS: I' + str(count) + ' max length 50 character'
        messages.append(message)

    # question_type
    if question_type is None:
        message = 'SHEET QUESTIONS: J' + str(count) + ' is not NULL'
        messages.append(message)

    if question_type is not None and type(question_type) not in [int]:
        message = 'SHEET QUESTIONS: J' + str(count) + ' is not character (string)'
        messages.append(message)

    # question_label
    if question_label is not None and len(question_label) > 254:
        message = 'SHEET QUESTIONS: K' + str(count) + ' max length 254 character'
        messages.append(message)

    # sub_question_code
    if sub_question_code is not None and len(sub_question_code) > 50:
        message = 'SHEET QUESTIONS: L' + str(count) + ' max length 50 character'
        messages.append(message)

    # sub_question
    if sub_question is not None and len(sub_question) > 254:
        message = 'SHEET QUESTIONS: M' + str(count) + ' max length 254 character'
        messages.append(message)

    # question_ordering
    if question_ordering is None:
        message = 'SHEET QUESTIONS: N' + str(count) + ' is not NULL'
        messages.append(message)

    # question_ordering
    if question_ordering is not None and type(question_ordering) not in [int]:
        message = 'SHEET QUESTIONS: N' + str(count) + ' is not character (string)'
        messages.append(message)

    # option_code
    if option_code is None:
        message = 'SHEET QUESTIONS: O' + str(count) + ' is not NULL'
        messages.append(message)

    # option_code
    if option_code is not None and len(option_code) > 50:
        message = 'SHEET QUESTIONS: O' + str(count) + ' max length 50 character'
        messages.append(message)

    # option_name
    if option_name is not None and len(option_name) > 254:
        message = 'SHEET QUESTIONS: P' + str(count) + ' max length 254 character'
        messages.append(message)

    # required_flag
    if required_flag is not None and type(required_flag) not in [int]:
        message = 'SHEET QUESTIONS: Q' + str(count) + ' is not character (string)'
        messages.append(message)

    # answers_ordering
    if option_ordering is None:
        message = 'SHEET QUESTIONS: R' + str(count) + ' is not NULL'
        messages.append(message)

    if option_ordering is not None and type(option_ordering) not in [int]:
        message = 'SHEET QUESTIONS: R' + str(count) + ' is not character (string)'
        messages.append(message)

    if len(messages) > 0:
        write_log(messages)

    return messages


def process_sheet_questions(data):
    """
    process_sheet_questions
    :param data:
    :return:
    """
    count = 0
    for row in data:
        row_data = dict()
        count = count + 1
        if count > 1:
            for cell in row:
                name = cell.column_letter
                value_cell = cell.value
                row_data[name] = value_cell

            errors_row = validate_row_questions(count, row_data)
            if len(errors_row) == 0:
                question_id = row_data.get('A')
                questionnaire_id = row_data.get('B')
                language = row_data.get('C')
                description = row_data.get('D')
                title_code = row_data.get('E')
                title_name = row_data.get('F')
                question = row_data.get('G')
                title_ordering = row_data.get('H')
                question_code = row_data.get('I')
                question_type = row_data.get('J')
                question_label = row_data.get('K')
                sub_question_code = row_data.get('L')
                sub_question = row_data.get('M')
                question_ordering = row_data.get('N')
                option_code = row_data.get('O')
                option_name = row_data.get('P')
                required_flag = row_data.get('Q')
                option_ordering = row_data.get('R')
                data_insert = {
                    'question_id': question_id,
                    'questionnaire_id': questionnaire_id,
                    'language': language,
                    'description': description,
                    'title_code': title_code,
                    'title_name': title_name,
                    'question': question,
                    'title_ordering': title_ordering,
                    'question_code': question_code,
                    'question_type': question_type,
                    'question_label': question_label,
                    'sub_question_code': sub_question_code,
                    'sub_question': sub_question,
                    'question_ordering': question_ordering,
                    'option_code': option_code,
                    'option_name': option_name,
                    'required_flag': required_flag,
                    'option_ordering': option_ordering,
                }
                try:
                    obj = Questions.objects.get(pk=question_id,
                                                title_code=title_code,
                                                question_code=question_code,
                                                option_code=option_code)
                    for key, value in data_insert.items():
                        setattr(obj, key, value)
                    obj.save()
                except Questions.DoesNotExist:
                    obj = Questions(**data_insert)
                    obj.save()
            else:
                logging.info(row_data)


def validate_row_answers(count, row_data):
    """
    validate_row_answers
    :param count:
    :param row_data:
    :return:
    """
    messages = []
    answer_id = row_data.get('A')
    nhs_number = row_data.get('B')
    questionnaire_ldatetime = row_data.get('C')
    question_id = row_data.get('D')
    answer = str(row_data.get('E'))

    # answer_id
    if answer_id is None:
        message = 'SHEET ANSWERS: A' + str(count) + ' is not NULL'
        messages.append(message)

    if answer_id is not None and type(answer_id) not in [int]:
        message = 'SHEET ANSWERS: A' + str(count) + ' is not character (string)'
        messages.append(message)

    # nhs_number
    if nhs_number is None:
        message = 'SHEET ANSWERS: B' + str(count) + ' is not NULL'
        messages.append(message)
    else:
        try:
            Patient.objects.get(nhs_number=nhs_number)
        except Patient.DoesNotExist:
            message = 'SHEET ANSWERS: B' + str(count) + ' Patient does not exits'
            messages.append(message)

    # questionnaire_ldatetime
    if questionnaire_ldatetime is not None:
        if isinstance(questionnaire_ldatetime, datetime.datetime) is False:
            try:
                datetime.datetime.strptime(questionnaire_ldatetime, '%Y-%m-%d')
            except ValueError:
                message = 'SHEET ANSWERS: C' + str(count) + 'Incorrect data format, should be YYYY-MM-DD'
                messages.append(message)

    # question_id
    if question_id is None:
        message = 'SHEET ANSWERS: D' + str(count) + ' is not NULL'
        messages.append(message)

    if question_id is not None and type(question_id) not in [int]:
        message = 'SHEET ANSWERS: D' + str(count) + ' is not character (string)'
        messages.append(message)

    # answer
    if answer is not None and len(answer) > 254:
        message = 'SHEET ANSWERS: E' + str(count) + ' max length 254 character'
        messages.append(message)

    if len(messages) > 0:
        write_log(messages)

    return messages


def process_sheet_answers(data):
    """
    process_sheet_answers
    :param data:
    :return:
    """
    count = 0
    for row in data:
        row_data = dict()
        count = count + 1
        if count > 1:
            for cell in row:
                name = cell.column_letter
                value_cell = cell.value
                row_data[name] = value_cell
            errors_row = validate_row_answers(count, row_data)
            if len(errors_row) == 0:
                answer_id = row_data.get('A')
                nhs_number = row_data.get('B')
                questionnaire_ldatetime = row_data.get('C')
                question_id = row_data.get('D')
                answer = str(row_data.get('E'))
                try:
                    patient = Patient.objects.filter(nhs_number=nhs_number).first()
                    data_insert = {
                        'answer_id': answer_id,
                        'patient_id': patient.patient_id,
                        'questionnaire_ldatetime': questionnaire_ldatetime,
                        'question_id': question_id,
                        'answer': answer,
                    }
                    obj = Answers.objects.get(patient_id=patient.patient_id,
                                              questionnaire_ldatetime=questionnaire_ldatetime,
                                              )
                    logging.info(data_insert)
                    for key, value in data_insert.items():
                        setattr(obj, key, value)
                    obj.save()
                except Answers.DoesNotExist:
                    obj = Answers(**data_insert)
                    obj.save()
            else:
                logging.info(row_data)


def validate_row_patient_questionnaires(count, row_data):
    """
    validate_row_patient_questionnaires
    :param count:
    :param row_data:
    :return:
    """
    messages = []
    id = row_data.get('A')
    nhs_number = row_data.get('B')
    questionnaire_code = row_data.get('C')
    questionnaire_type = str(row_data.get('D'))
    questionnaire_date = row_data.get('E')
    status = str(row_data.get('F'))
    sync_date = row_data.get('G')

    # id
    if id is None:
        message = 'SHEET PATIENT_QUESTIONNAIRES: A' + str(count) + ' is not NULL'
        messages.append(message)

    if id is not None and type(id) not in [int]:
        message = 'SHEET PATIENT_QUESTIONNAIRES: A' + str(count) + ' is not character (string)'
        messages.append(message)

    # nhs_number
    if nhs_number is None:
        message = 'SHEET PATIENT_QUESTIONNAIRES: B' + str(count) + ' is not NULL'
        messages.append(message)
    else:
        try:
            Patient.objects.filter(nhs_number=nhs_number).first()
        except Patient.DoesNotExist:
            message = 'SHEET PATIENT_QUESTIONNAIRES: B' + str(count) + ' Patient does not exits'
            messages.append(message)

    # questionnaire_code
    if questionnaire_code is None:
        message = 'SHEET PATIENT_QUESTIONNAIRES: C' + str(count) + ' is not NULL'
        messages.append(message)
    else:
        try:
            Questionnaires.objects.get(code=questionnaire_code)
        except Questionnaires.DoesNotExist:
            message = 'SHEET PATIENT_QUESTIONNAIRES: C' + str(count) + ' Questionnaires does not exits'
            messages.append(message)

    # questionnaire_type
    if questionnaire_type is None:
        message = 'SHEET PATIENT_QUESTIONNAIRES: D' + str(count) + ' is not NULL'
        messages.append(message)

    if questionnaire_type is not None and len(questionnaire_type) > 20:
        message = 'SHEET PATIENT_QUESTIONNAIRES: D' + str(count) + ' max length 100 character'
        messages.append(message)

    # questionnaire_date
    if questionnaire_date is None:
        message = 'SHEET PATIENT_QUESTIONNAIRES: E' + str(count) + ' is not NULL'
        messages.append(message)

    if questionnaire_date is not None:
        if isinstance(questionnaire_date, datetime.datetime) is False:
            try:
                datetime.datetime.strptime(questionnaire_date, '%Y-%m-%d')
            except ValueError:
                message = 'SHEET PATIENT_QUESTIONNAIRES: E' + str(count) + 'Incorrect data format, should be YYYY-MM-DD'
                messages.append(message)

    if status is None:
        message = 'SHEET PATIENT_QUESTIONNAIRES: F' + str(count) + ' is not NULL'
        messages.append(message)

    if status is not None and type(status) not in [int]:
        message = 'SHEET PATIENT_QUESTIONNAIRES: F' + str(count) + ' is not character (string)'
        messages.append(message)

    # sync_date
    if sync_date is not None:
        if isinstance(sync_date, datetime.datetime) is False:
            try:
                datetime.datetime.strptime(sync_date, '%Y-%m-%d')
            except ValueError:
                message = 'SHEET ANSWERS: G' + str(count) + 'Incorrect data format, should be YYYY-MM-DD'
                messages.append(message)

    if len(messages) > 0:
        write_log(messages)

    return messages


class Command(BaseCommand):
    """
        Command import questions
    """
    help = 'Import Questions'

    def add_arguments(self, parser):
        parser.add_argument('-f', '--file_excel', type=str, help='file excel')

    def handle(self, *args, **kwargs):
        file_excel = kwargs['file_excel']
        if os.path.isfile(file_excel) is True:
            # sheet list_user
            file_excel = kwargs['file_excel']
            worksheet = openpyxl.load_workbook(file_excel)

            # QUESTIONNAIRES
            logging.info('SHEET QUESTIONNAIRES')
            worksheet_questionnaires = worksheet["QUESTIONNAIRES"]
            data_questionnaires = worksheet_questionnaires.iter_rows()
            process_sheet_questionnaires(data_questionnaires)
            logging.info('END SHEET QUESTIONNAIRES')

            # QUESTIONS
            logging.info('SHEET QUESTIONS')
            worksheet_questions = worksheet["QUESTIONS"]
            data_questions = worksheet_questions.iter_rows()
            process_sheet_questions(data_questions)
            logging.info('END SHEET QUESTIONS')

            # ANSWERS
            logging.info('SHEET ANSWERS')
            worksheet_answers = worksheet["ANSWERS"]
            data_answers = worksheet_answers.iter_rows()
            process_sheet_answers(data_answers)
            logging.info('END SHEET ANSWERS')

        else:
            logging.error('Path file is not exits!')
            self.stdout.write(self.style.ERROR('Path file is not exits!'))
