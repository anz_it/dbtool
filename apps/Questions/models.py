"""
Define  Questionnaires , Questions, Answers Models
"""
from datetime import datetime

from django.db import models
from django.db.models import Max, F


class Questionnaires(models.Model):
    """
    Questionnaires Model
    """
    questionnaire_id = models.AutoField(primary_key=True)
    version = models.CharField(max_length=20)
    type = models.CharField(max_length=30)
    status = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=datetime.now, db_index=True)
    updated_date = models.DateTimeField(auto_now=True, db_index=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            max = Questionnaires.objects.aggregate(max=Max(F('questionnaire_id')))['max']
            self.id = max + 2 if max else 1  # if the DB is empty
        super().save(*args, **kwargs)

    class Meta:
        db_table = "questionnaires"


# Create your models here.
class Questions(models.Model):
    """
    Questions Model
    """
    question_id = models.AutoField(primary_key=True)
    questionnaire_id = models.IntegerField()
    language = models.CharField(max_length=5)
    description = models.CharField(max_length=1000)
    title_code = models.CharField(max_length=50)
    title_name = models.CharField(null=True, max_length=100)
    question = models.CharField(max_length=254, null=True)
    title_ordering = models.IntegerField(null=True)
    question_code = models.CharField(max_length=50)
    question_type = models.IntegerField()
    question_label = models.CharField(max_length=254, null=True)
    sub_question_code = models.CharField(max_length=50)
    sub_question = models.CharField(max_length=254, null=True)
    question_ordering = models.IntegerField(null=True)
    option_code = models.CharField(max_length=50)
    option_name = models.CharField(max_length=254)
    required_flag = models.IntegerField(null=True)
    option_ordering = models.IntegerField(null=True)
    created_date = models.DateTimeField(default=datetime.now, db_index=True)
    updated_date = models.DateTimeField(auto_now=True, db_index=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            max = Questions.objects.aggregate(max=Max(F('question_id')))['max']
            self.id = max + 2 if max else 1  # if the DB is empty
        super().save(*args, **kwargs)

    class Meta:
        db_table = "questions"


class Answers(models.Model):
    """
    Answers Model
    """
    answer_id = models.AutoField(primary_key=True)
    patient_id = models.CharField(max_length=255)
    questionnaire_ldatetime = models.DateTimeField()
    question_id = models.IntegerField()
    answer = models.TextField()
    created_date = models.DateTimeField(default=datetime.now, db_index=True)
    updated_date = models.DateTimeField(auto_now=True, db_index=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            max = Answers.objects.aggregate(max=Max(F('answer_id')))['max']
            self.id = max + 2 if max else 1  # if the DB is empty
        super().save(*args, **kwargs)

    class Meta:
        db_table = "answers"
