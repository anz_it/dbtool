"""
extended_choices
"""
import extended_choices

TARGET_PATIENT = extended_choices.Choices(
    ('ADULT', 1, 'Adult'),
    ('ELDER', 2, 'Elder'),
)
