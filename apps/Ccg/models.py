"""
CCG Models
"""
import datetime
from django.db import models

class Ccg(models.Model):
    """
    Ccg models
    """
    ccg_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=50)
    status = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=datetime.datetime.now, db_index=True)
    updated_date = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        """Meta"""
        db_table = "ccgs"
        def __ceil__(self):
            pass

        def __str__(self):
            return self.__class__.__name__