""" Import Medicines """
import os.path
import datetime
import logging
from pathlib import Path
from django.core.management.base import BaseCommand
import openpyxl
from apps.Ccg import consts
from apps.Ccg.models import Ccg
from apps.Medicines.models import Medicines
from apps.MedicinesCategories.models import MedicinesCategories
from apps.MedicineActives.models import MedicineActives


def get_category(ccg_id, data):
    """
    get_category
    :param ccg_id:
    :param data:
    :return:
    """
    category_excel = data.get('E')
    if category_excel is not None:
        try:
            category = MedicinesCategories.objects.get(ccg_id=ccg_id, name=category_excel)
            return category
        except MedicinesCategories.DoesNotExist:
            data_insert = {
                'ccg_id': ccg_id,
                'language': 'en',
                'name': category_excel,
                'argo_category_name': category_excel,
                'ordering': None,
            }
            obj = MedicinesCategories(**data_insert)
            obj.save()
            return obj


def get_active(ccg_id, data):
    """
    get_active
    :param ccg_id:
    :param data:
    :return:
    """
    category_obj = get_category(ccg_id, data)
    active_excel = data.get('G')
    try:
        active = MedicineActives.objects.get(ccg_id=ccg_id, category_id=category_obj.category_id, name=active_excel)
        return active
    except MedicineActives.DoesNotExist:
        data_insert = {
            'ccg_id': ccg_id,
            'category_id': category_obj.category_id,
            'language': 'en',
            'name': active_excel,
            'ordering': None,
        }
        obj = MedicineActives(**data_insert)
        obj.save()
        return obj


def get_category_from_excel(ccg_id, data):
    """
    get_category_from_excel
    :param ccg_id:
    :param data:
    :return:
    """
    category = get_category(ccg_id, data)
    return {'category_id': category.category_id}


def get_active_from_excel(ccg_id, data):
    """
    get_active_from_excel
    :param ccg_id:
    :param data:
    :return:
    """
    active_obj = get_active(ccg_id, data)
    return {'active_id': active_obj.active_id}


def get_level_from_excel(data):
    """
    get_level_from_excel
    :param data:
    :return:
    """
    levels_adults = list()
    levels_elderly = list()
    adults_initial = data.get('K')
    adults_medium = data.get('O')
    adults_high = data.get('T')
    adults_maximum = data.get('Y')
    if adults_initial is not None:
        data_dict = {
            'level': '',
            'dose': '',
            'frequency': '',
            'target_patient': consts.TARGET_PATIENT.ADULT,
        }
        level = {'level': 'Initial'}
        level_dose = {'dose': data.get('K')}
        one_per_day = data.get('L')
        twice_per_day = data.get('M')
        three_per_day = data.get('N')
        four_per_day = None
        frequency = {'frequency': get_frequency_from_excel(one_per_day, twice_per_day, three_per_day, four_per_day)}
        data_dict.update(level)
        data_dict.update(level_dose)
        data_dict.update(frequency)
        levels_adults.append(data_dict)

    if adults_medium is not None:
        data_dict = {
            'level': '',
            'dose': '',
            'frequency': '',
            'target_patient': consts.TARGET_PATIENT.ADULT,
        }
        level = {'level': 'Medium'}
        level_dose = {'dose': data.get('O')}
        one_per_day = data.get('P')
        twice_per_day = data.get('Q')
        three_per_day = data.get('R')
        four_per_day = data.get('S')
        frequency = {'frequency': get_frequency_from_excel(one_per_day, twice_per_day, three_per_day, four_per_day)}
        data_dict.update(level)
        data_dict.update(level_dose)
        data_dict.update(frequency)
        levels_adults.append(data_dict)

    if adults_high is not None:
        data_dict = {
            'level': '',
            'dose': '',
            'frequency': '',
            'target_patient': consts.TARGET_PATIENT.ADULT,
        }
        level = {'level': 'High'}
        level_dose = {'dose': data.get('T')}
        one_per_day = data.get('U')
        twice_per_day = data.get('V')
        three_per_day = data.get('W')
        four_per_day = data.get('X')
        frequency = {'frequency': get_frequency_from_excel(one_per_day, twice_per_day, three_per_day, four_per_day)}
        data_dict.update(level)
        data_dict.update(level_dose)
        data_dict.update(frequency)
        levels_adults.append(data_dict)

    if adults_maximum is not None:
        data_dict = {
            'level': '',
            'dose': '',
            'frequency': '',
            'target_patient': consts.TARGET_PATIENT.ADULT,
        }
        level = {'level': 'Maximum'}
        level_dose = {'dose': data.get('Y')}
        one_per_day = data.get('Z')
        twice_per_day = data.get('AA')
        three_per_day = data.get('AB')
        four_per_day = data.get('AC')
        frequency = {'frequency': get_frequency_from_excel(one_per_day, twice_per_day, three_per_day, four_per_day)}
        data_dict.update(level)
        data_dict.update(level_dose)
        data_dict.update(frequency)
        levels_adults.append(data_dict)

    ############################# elderly ################################

    elderly_initial = data.get('AD')
    elderly_medium = data.get('AH')
    elderly_high = data.get('AM')
    elderly_maximum = data.get('AR')

    if elderly_initial is not None:
        data_dict = {
            'level': '',
            'dose': '',
            'frequency': '',
            'target_patient': consts.TARGET_PATIENT.ELDER,
        }
        level = {'level': 'Initial'}
        level_dose = {'dose': data.get('AD')}
        one_per_day = data.get('AE')
        twice_per_day = data.get('AF')
        three_per_day = data.get('AG')
        four_per_day = None
        frequency = {'frequency': get_frequency_from_excel(one_per_day, twice_per_day, three_per_day, four_per_day)}
        data_dict.update(level)
        data_dict.update(level_dose)
        data_dict.update(frequency)
        levels_elderly.append(data_dict)

    if elderly_medium is not None:
        data_dict = {
            'level': '',
            'dose': '',
            'frequency': '',
            'target_patient': consts.TARGET_PATIENT.ELDER,
        }
        level = {'level': 'Medium'}
        level_dose = {'dose': data.get('AH')}
        one_per_day = data.get('AI')
        twice_per_day = data.get('AJ')
        three_per_day = data.get('AK')
        four_per_day = data.get('AL')
        frequency = {'frequency': get_frequency_from_excel(one_per_day, twice_per_day, three_per_day, four_per_day)}
        data_dict.update(level)
        data_dict.update(level_dose)
        data_dict.update(frequency)
        levels_elderly.append(data_dict)

    if elderly_high is not None:
        data_dict = {
            'level': '',
            'dose': '',
            'frequency': '',
            'target_patient': consts.TARGET_PATIENT.ELDER,
        }
        level = {'level': 'High'}
        level_dose = {'dose': data.get('AM')}
        one_per_day = data.get('AN')
        twice_per_day = data.get('AO')
        three_per_day = data.get('AP')
        four_per_day = data.get('AQ')
        frequency = {'frequency': get_frequency_from_excel(one_per_day, twice_per_day, three_per_day, four_per_day)}
        data_dict.update(level)
        data_dict.update(level_dose)
        data_dict.update(frequency)
        levels_elderly.append(data_dict)

    if elderly_maximum is not None:
        data_dict = {
            'level': '',
            'dose': '',
            'frequency': '',
            'target_patient': consts.TARGET_PATIENT.ELDER,
        }
        level = {'level': 'Maximum'}
        level_dose = {'dose': data.get('AR')}
        one_per_day = data.get('AS')
        twice_per_day = data.get('AT')
        three_per_day = data.get('AU')
        four_per_day = data.get('AV')
        frequency = {'frequency': get_frequency_from_excel(one_per_day, twice_per_day, three_per_day, four_per_day)}
        data_dict.update(level)
        data_dict.update(level_dose)
        data_dict.update(frequency)
        levels_elderly.append(level)

    return levels_adults, levels_elderly


def get_brand_from_excel(data):
    """
    get_brand_from_excel
    :param data:
    :return:
    """
    return {'brand': data.get('I')}


def get_form_from_excel(data):
    """
    get_form_from_excel
    :param data:
    :return:
    """
    data_insert = []
    form_tablet_1 = data.get('AW')
    form_tablet_2 = data.get('AX')
    form_tablet_3 = data.get('AY')
    form_tablet_4 = data.get('AZ')
    form_tablet_5 = data.get('BA')
    form_tablet_6 = data.get('BB')
    if form_tablet_1 is not None or form_tablet_2 is not None or form_tablet_3 is not None or form_tablet_4 is not None or form_tablet_5 is not None or form_tablet_6 is not None:
        form_data = {'form': 'Tablet'}
        data_insert.append(form_data)

    form_release_tablet_1 = data.get('BC')
    form_release_tablet_2 = data.get('BD')
    form_release_tablet_3 = data.get('BE')
    form_release_tablet_4 = data.get('BF')

    if form_release_tablet_1 is not None or form_release_tablet_2 is not None or form_release_tablet_3 is not None \
            or form_release_tablet_4 is not None:
        form_tablet = {'form': 'Modified Release Tablet'}
        data_insert.append(form_tablet)

    form_oral_solution_1 = data.get('BG')
    form_oral_solution_2 = data.get('BH')
    form_oral_solution_3 = data.get('BI')
    form_oral_solution_4 = data.get('BJ')
    if form_oral_solution_1 is not None or form_oral_solution_2 is not None or form_oral_solution_3 is not None or form_oral_solution_4 is not None:
        form_tablet = {'form': 'Oral Solution'}
        data_insert.append(form_tablet)

    form_oral_suspension_1 = data.get('BK')
    form_oral_suspension_2 = data.get('BL')
    form_oral_suspension_3 = data.get('BM')
    form_oral_suspension_4 = data.get('BN')
    if form_oral_suspension_1 is not None or form_oral_suspension_2 is not None or form_oral_suspension_3 is not None or form_oral_suspension_4 is not None:
        form_tablet = {'form': 'Oral Suspension'}
        data_insert.append(form_tablet)

    form_capsule_1 = data.get('BO')
    form_capsule_2 = data.get('BP')
    form_capsule_3 = data.get('BQ')
    form_capsule_4 = data.get('BR')
    if form_capsule_1 is not None or form_capsule_2 is not None or form_capsule_3 is not None or form_capsule_4 is not None:
        form_tablet = {'form': 'Capsule'}
        data_insert.append(form_tablet)

    form_release_capsule_1 = data.get('BS')
    form_release_capsule_2 = data.get('BT')
    form_release_capsule_3 = data.get('BU')
    form_release_capsule_4 = data.get('BV')
    form_release_capsule_5 = data.get('BW')
    if form_release_capsule_1 is not None or form_release_capsule_2 is not None or form_release_capsule_3 is not None \
            or form_release_capsule_4 is not None or form_release_capsule_5 is not None:
        form_tablet = {'form': 'Modified release Capsule'}
        data_insert.append(form_tablet)

    return data_insert


def get_frequency_from_excel(one_per_day, twice_per_day, three_per_day, four_per_day):
    """
    get_frequency_from_excel
    :param one_per_day:
    :param twice_per_day:
    :param three_per_day:
    :param four_per_day:
    :return:
    """
    frequency = {
        'one_per_day': 0,
        'twice_per_day': 0,
        'three_per_day': 0,
        'four_per_day': 0,
    }

    if one_per_day is not None:
        one_per_day = {
            'one_per_day': 1,
        }
        frequency.update(one_per_day)

    if twice_per_day is not None:
        twice_per_day = {
            'twice_per_day': 1,
        }
        frequency.update(twice_per_day)

    if three_per_day is not None:
        three_per_day = {
            'three_per_day': 1,
        }
        frequency.update(three_per_day)
    if four_per_day is not None:
        four_per_day = {
            'four_per_day': 1,
        }
        frequency.update(four_per_day)

    return frequency


def get_titration_note_adults_from_excel(data):
    """
    get_titration_note_adults_from_excel
    :param data:
    :return:
    """
    return {'titration_note_adults': data.get('BX')}


def get_monitoring_from_excel(data):
    """
    get_monitoring_from_excel
    :param data:
    :return:
    """
    return {'monitoring': data.get('BY')}


def get_renal_impairment_from_excel(data):
    """
    get_renal_impairment_from_excel
    :param data:
    :return:
    """
    return {'renal_impairment': data.get('BZ')}


def get_cautions_from_excel(data):
    """
    get_cautions_from_excel
    :param data:
    :return:
    """
    return {'cautions': data.get('CA')}


def get_diabetes_from_excel(data):
    """
    get_diabetes_from_excel
    :param data:
    :return:
    """
    return {'diabetes': data.get('CB')}


def get_heart_failure_from_excel(data):
    """
    get_heart_failure_from_excel
    :param data:
    :return:
    """
    return {'heart_failure': data.get('CD')}


def get_all_contraindications_from_excel(data):
    """
    get_all_contraindications_from_excel
    :param data:
    :return:
    """
    return {'all_contraindications': data.get('CE')}


def get_status():
    """
    get_status
    :return:
    """
    return {'status': 1}


def get_ordering_from_excel(data):
    """
    get_ordering_from_excel
    :param data:
    :return:
    """
    return {'ordering': data.get('B')}


def validate_row(count, data):
    """
    validate_row
    :param count:
    :param data:
    :return:
    """
    messages = []
    # category
    if data.get('E') is None:
        message = ' Row:E' + str(count) + ' Category column must not be null values'
        messages.append(message)

    if data.get('E') is not None and len(data.get('E')) > 254:
        message = ' Row:E' + str(count) + ' maximum 254 character'
        messages.append(message)
    # active
    if data.get('G') is None:
        message = ' Row:G' + str(count) + ' Active column must not be null values'
        messages.append(message)

    if data.get('G') is not None and len(data.get('G')) > 254:
        message = ' Row:G' + str(count) + ' maximum 254 character'
        messages.append(message)
    # branch - name
    if data.get('I') is None:
        message = ' Row:I' + str(count) + ' Branch column must not be null values'
        messages.append(message)

    if data.get('I') is not None and len(data.get('I')) > 254:
        message = ' Row:I' + str(count) + ' maximum 254 character'
        messages.append(message)

    # Level
    if (data.get('K') is None and data.get('O') is None and data.get('T') is None and data.get('Y') is None) and \
            (data.get('AD') is None and data.get('AH') is None and data.get('AM') is None and data.get('AR') is None):
        message = ' Row:K' + str(count) + '/O' + str(count) + '/T' + str(count) + '/Y' + str(
            count) + ' all the empty rows together'
        messages.append(message)

        message = ' Row:AD' + str(count) + '/AH' + str(count) + '/AM' + str(count) + '/AR' + str(
            count) + ' all the empty rows together'
        messages.append(message)

    return messages


def get_logging_config():
    """
        get_logging_config
    :return:
    """
    path_folder = str(Path().absolute())
    path = str(path_folder + '/logs/import-medicines_' + datetime.datetime.now().strftime('%d-%m-%Y') + '.log')
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.ERROR,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%d-%m-%Y %H:%M',
                        filename=path,
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    return logging


def write_log(logfile, messages):
    """
    write_log
    :param logfile:
    :param messages:
    """
    for message in messages:
        logfile.error(message)


class Command(BaseCommand):
    """Import excel medicines"""
    help = 'Import excel medicines'

    def add_arguments(self, parser):
        parser.add_argument('-ccg_code', '--ccg_code', type=str, help='ccg code')
        parser.add_argument('-file_excel', '--file_excel', type=str, help='file excel')

    def handle(self, *args, **kwargs):
        logfile = get_logging_config()
        file_excel = kwargs['file_excel']
        ccg_code = kwargs['ccg_code']
        if ccg_code is None:
            message = 'please input ccg code'
            self.stdout.write(self.style.ERROR(message))
            return False

        if os.path.isfile(file_excel) is True:
            file_excel = kwargs['file_excel']
            errors_messages = []
            errors_row = []
            try:
                worksheet_load = openpyxl.load_workbook(file_excel)
                # getting a particular sheet by name out of many sheets
                worksheet = worksheet_load.active
                ccg = Ccg.objects.get(code=ccg_code)
            except FileNotFoundError:
                message = 'Medicince file is not exist in system.'
                errors_row.append(str(message))
                errors_messages.append(errors_row)
                self.stdout.write(self.style.ERROR(message))
                return False
            except Ccg.DoesNotExist:
                message = 'Ccg code is not exist in system.'
                self.stdout.write(self.style.ERROR(message))
                return False
            except Exception as message:
                errors_row.append(str(message))
                errors_messages.append(errors_row)
                self.stdout.write(self.style.ERROR(message))
                return False

            count = 0
            for row in worksheet.iter_rows():
                row_data = dict()
                count = count + 1
                self.stdout.write(self.style.NOTICE('processing ... Row' + str(count)))
                if count > 4:
                    for cell in row:
                        name = cell.column_letter
                        value_cell = cell.value
                        row_data[name] = value_cell

                    errors_row = validate_row(count, row_data)
                    forms = get_form_from_excel(row_data)
                    if not forms:
                        message = 'AW' + str(count) + '->' + 'BB' + str(count) + \
                                  ', BC' + str(count) + '->' + 'BF' + str(count) + \
                                  ', BG' + str(count) + '->' + 'BJ' + str(count) + \
                                  ', BK' + str(count) + '->' + 'BN' + str(count) + \
                                  ', BO' + str(count) + '->' + 'BR' + str(count) + \
                                  ', BS' + str(count) + '->' + 'BW' + str(count) + \
                                  ' all the empty rows together'
                        errors_row.append(message)
                    if len(errors_row) > 0:
                        write_log(logfile, errors_row)
                        continue

                    data_insert = {
                        'ccg_id': ccg.ccg_id,
                        'category_id': '',
                        'level': '',
                        'active_id': '',
                        'brand': '',
                        'form': '',
                        'dose': '',
                        'frequency': '',
                        'target_patient': '',
                        'titration_note_adults': '',
                        'monitoring': '',
                        'renal_impairment': '',
                        'cautions': '',
                        'diabetes': '',
                        'heart_failure': '',
                        'all_contraindications': '',
                        'status': '',
                        'ordering': '',
                    }
                    for form in forms:
                        data_insert.update(form)
                        levels_adults, levels_elderly = get_level_from_excel(row_data)
                        for level_dose_frequency_target_patient in levels_adults:
                            # level / dose/ frequency /target_patient
                            data_insert.update(level_dose_frequency_target_patient)
                            # category
                            data_insert.update(get_category_from_excel(ccg.ccg_id, row_data))
                            # active
                            data_insert.update(get_active_from_excel(ccg.ccg_id, row_data))
                            # Branch - Name
                            data_insert.update(get_brand_from_excel(row_data))
                            data_insert.update(get_titration_note_adults_from_excel(row_data))
                            data_insert.update(get_monitoring_from_excel(row_data))
                            data_insert.update(get_renal_impairment_from_excel(row_data))
                            data_insert.update(get_cautions_from_excel(row_data))
                            data_insert.update(get_diabetes_from_excel(row_data))
                            data_insert.update(get_heart_failure_from_excel(row_data))
                            data_insert.update(get_all_contraindications_from_excel(row_data))
                            data_insert.update(get_status())
                            # Ordering
                            data_insert.update(get_ordering_from_excel(row_data))

                            try:
                                obj = Medicines.objects.get(ccg_id=ccg.ccg_id,
                                                            category_id=data_insert.get('category_id'),
                                                            level=data_insert.get('level'),
                                                            active_id=data_insert.get('active_id'),
                                                            brand=data_insert.get('brand'),
                                                            form=data_insert.get('form'),
                                                            target_patient=data_insert.get('target_patient')
                                                            )
                                try:
                                    for key, value in data_insert.items():
                                        setattr(obj, key, value)
                                    obj.save()
                                except Exception as errors_mes:
                                    logfile.error(str(errors_mes))
                                    continue
                            except Medicines.DoesNotExist:
                                obj = Medicines(**data_insert)
                                obj.save()

                        for level_dose_frequency_target_patient in levels_elderly:
                            # level / dose/ frequency /target_patient
                            data_insert.update(level_dose_frequency_target_patient)
                            # category
                            data_insert.update(get_category_from_excel(ccg.ccg_id, row_data))
                            # active
                            data_insert.update(get_active_from_excel(ccg.ccg_id, row_data))
                            # Branch - Name
                            data_insert.update(get_brand_from_excel(row_data))
                            data_insert.update(get_titration_note_adults_from_excel(row_data))
                            data_insert.update(get_monitoring_from_excel(row_data))
                            data_insert.update(get_renal_impairment_from_excel(row_data))
                            data_insert.update(get_cautions_from_excel(row_data))
                            data_insert.update(get_diabetes_from_excel(row_data))
                            data_insert.update(get_heart_failure_from_excel(row_data))
                            data_insert.update(get_all_contraindications_from_excel(row_data))
                            data_insert.update(get_status())
                            # Ordering
                            data_insert.update(get_ordering_from_excel(row_data))

                            try:
                                obj = Medicines.objects.get(ccg_id=ccg.ccg_id,
                                                            category_id=data_insert.get('category_id'),
                                                            level=data_insert.get('level'),
                                                            active_id=data_insert.get('active_id'),
                                                            brand=data_insert.get('brand'),
                                                            form=data_insert.get('form'),
                                                            target_patient=data_insert.get('target_patient')
                                                            )
                                try:
                                    for key, value in data_insert.items():
                                        setattr(obj, key, value)
                                    obj.save()
                                except Exception as errors_mes:
                                    logfile.error(str(errors_mes))
                                    continue
                            except Medicines.DoesNotExist:
                                obj = Medicines(**data_insert)
                                obj.save()

                self.stdout.write(self.style.NOTICE('Row :' + str(count)) + ' Done')

        else:
            logfile.error('Path file is not exits!')
            self.stdout.write(self.style.ERROR('Path file is not exits!'))
