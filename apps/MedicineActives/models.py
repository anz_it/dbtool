from django.db import models
from apps.Ccg.models import Ccg
from apps.MedicinesCategories.models import MedicinesCategories
from datetime import datetime


# Create your models here.
class MedicineActives(models.Model):
    active_id = models.AutoField(primary_key=True)
    ccg = models.ForeignKey(Ccg, on_delete=models.CASCADE, related_name='Ccg_Medicine_Actives')
    category_id = models.IntegerField(null=False, blank=False)
    language = models.CharField(max_length=20)
    name = models.CharField(max_length=254)
    ordering = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=datetime.now, db_index=True, blank=True, null=True)
    updated_date = models.DateTimeField(auto_now=True, db_index=True, blank=True, null=True)

    class Meta:
        db_table = "medicine_actives"
