from django.apps import AppConfig


class MedicineactivesConfig(AppConfig):
    name = 'MedicineActives'
