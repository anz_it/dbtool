#install / ubuntu / 
$ sudo apt update
$ sudo apt -y upgrade
$ sudo apt-get install libmysqlclient-dev

#centos
$ sudo yum install -y python36-devel mysql-devel gcc

#install python 
$ python --version
$ sudo apt install python3-pip -y
$ pip3 install virtualenv
$ virtualenv --python=python3 venv
$ . venv/bin/activate

#create app
$ python manage.py startapp AppName apps/AppName

#run console command line
$ python manage.py delete_users 1 

#List console command line
#-import medicines
$ python manage.py import_medicines -c 'CCG1' -f '/home/ominext/Documents/GpTool/templates/excel/BNF_CCG Brighton and Hove.xlsx'

#-import account dashboard
$ python manage.py account_dashboard -f '/home/hoa/Documents/GP/3.4.Source-Tool-Batch/templates/excel/Account Dashboard.xlsx'

#-import account patients
$ python manage.py account_patients -f '/home/hoa/Documents/GP/3.4.Source-Tool-Batch/templates/excel/Ex2.xlsx'

#-import questions
$ python manage.py import-questions -f '/home/hoa/Documents/GP/3.4.Source-Tool-Batch/templates/excel/question.xlsx'

#-import action card
$ python manage.py import-action-cards -f '/home/hoa/Documents/GP/3.4.Source-Tool-Batch/templates/excel/action-cart.xlsx'

#-import reminders
$ python manage.py import-reminders -f '/home/hoa/Documents/GP/3.4.Source-Tool-Batch/templates/excel/reminders.xlsx'